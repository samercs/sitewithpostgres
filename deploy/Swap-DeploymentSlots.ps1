﻿
$websiteName = "Sehetek"

Try
{
    $website = Get-AzureWebsite -Name $websiteName -ErrorAction Stop;
}
Catch [System.ArgumentException]
{
    if ($_.Exception.Message -eq "Your Azure credentials have not been set up or have expired, please run Add-AzureAccount to set up your Azure credentials.")
    {
        Add-AzureAccount;

        Write-Host "Azure account set, re-run script to continue."
    }
    else
    {
        Write-Host $_.Exception.Message;
    }
}


Switch-AzureWebsiteSlot –Name $websiteName -Slot1 staging -Slot2 production