﻿$target = Read-Host "Deploy to staging (s) production (p)?";

$raygunAuthorizationToken = "3GGEvvGLFcyMDHsRlHpSJJQqjiv9uYw2";
$raygunApiKey = "dhMim/ilOb7hVd6gHOEFLw==";

switch ($target)
{
    "s" 
    {
        $profile = "Staging";
        $username = "`$Sehetek__staging";
        $password = "bTqNylC1Yl7GChohpnrXeQmeFpCJX5FkLJQS2PLqx2xhCEo3oStzdTTvA4sh";
        $url = "https://sehetek-staging.azurewebsites.net";
    }
    "p" 
    {
        $profile = "Production";
        $username = "`$Sehetek";
        $password = "X3bqjSZFLra9dn43lh92hmtspQdk9PTlNNzS3lLlt96tl0jwTdNsL9mdrGXT";
        $url = "https://sehetek.azurewebsites.net";
    }
    default
    {
        Write-Host "Invalid deploy target!";
        return;
    }
}

function Log-Deployment
{
    if ($raygunAuthorizationToken -eq "" -Or $raygunApiKey -eq "")
    {
        return;
    }
    
    Remove-Module New-DeploymentLog -ErrorAction SilentlyContinue
    Import-Module ($currDirectory.Path + '\New-DeploymentLog.psm1') -Scope Local -Verbose:$false

    #$version = ((Get-Date).ToUniversalTime()).ToString("yyyyMMddTHHmmssZ")
    $gitShortHash = git.exe log -1 --pretty=format:%h 
    $gitFullHash = git.exe log -1 --pretty=format:%H
    $gitUser = git.exe config user.name
    $gitEmail = git.exe config user.email

    New-DeploymentLog -raygunAuthToken $raygunAuthorizationToken -raygunApiKey $raygunApiKey -version $gitShortHash -scmIdentifier $gitFullHash -ownerName $gitUser -emailAddress $gitEmail;
}

function Ping-Website
{
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory = $true)]
        [String]
        $Url
    )

    $url = $url + "/ok?" + (Get-Date).Ticks;
        
    Write-Host "Build completed successfully, pinging $url..." 

    $webClient = new-object System.Net.WebClient
    $webClient.Headers.Add("user-agent", "PowerShell Script")
    $webClient.DownloadString($url);
}

function Deploy-WebApp
{
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory = $true)]
        [String]
        $Profile,

        [Parameter(Mandatory = $true)]
        [String]
        $Username,

        [Parameter(Mandatory = $true)]
        [String]
        $Password 
    )
    
    $currDirectory = Resolve-Path .\ 
    $webProjectDirectory = Get-ChildItem -Path (Resolve-Path ..\.\src) | ? { $_.Name.EndsWith(".Web") };
    $webProjectFile = Get-ChildItem -Path $webProjectDirectory.FullName | ? {$_.Extension -eq ".csproj"} | Select-Object -First 1

    Remove-Module Invoke-MsBuild -ErrorAction SilentlyContinue; 
    Import-Module ($currDirectory.Path + '\Invoke-MsBuild.psm1') -Scope Local -Verbose:$false
    
    $params = "/property:Configuration=Release;PublishProfile={0};UserName={1};Password={2};DeployOnBuild=true;WarningLevel=1;AllowUntrustedCertificate=true;VisualStudioVersion=14.0 /verbosity:m" -f $profile, $username, $password
    $buildSucceeded = Invoke-MsBuild -Path $webProjectFile.FullName -ShowBuildWindowAndPromptForInputBeforeClosing -MsBuildParameters $params

    if ($buildSucceeded)
    { 
        Log-Deployment
        
        Ping-Website -Url $url

        Write-Host "Deploy completed."; 
    }
    else
    { 
        Write-Host "Build failed. Check the build log file for errors." 
    }
}

Deploy-WebApp -Profile $profile -Username $username -Password $password