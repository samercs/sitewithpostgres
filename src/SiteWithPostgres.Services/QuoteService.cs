using SiteWithPostgres.Data;
using SiteWithPostgres.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace SiteWithPostgres.Services
{
    public class QuoteService : ServiceBase
    {
        public QuoteService(IDataContextFactory dataContextFactory) : base(dataContextFactory)
        {
        }

        public async Task<IReadOnlyCollection<Quote>> GetAll()
        {
            using (var dc = DataContext())
            {
                return await dc.Quotes.ToListAsync();
            }
        }

        public async Task<Quote> Add(Quote quote)
        {
            using (var dc = DataContext())
            {
                dc.Quotes.Add(quote);
                await dc.SaveChangesAsync();
                return quote;
            }
        }

        public async Task<Quote> GetById(int id)
        {
            using (var dc = DataContext())
            {
                return await dc.Quotes.SingleOrDefaultAsync(i => i.QuoteId == id);
            }
        }

        public async Task<Quote> Edit(Quote quote)
        {
            using (var dc = DataContext())
            {
                dc.SetModified(quote);
                await dc.SaveChangesAsync();
                return quote;
            }
        }

        public async Task Remove(Quote quote)
        {
            using (var dc = DataContext())
            {
                dc.Quotes.Attach(quote);
                dc.Quotes.Remove(quote);
                await dc.SaveChangesAsync();
            }
        }
    }
}
