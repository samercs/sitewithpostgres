using SiteWithPostgres.Data;
using SiteWithPostgres.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace SiteWithPostgres.Services
{
    public class SlideService : ServiceBase
    {
        public SlideService(IDataContextFactory dataContextFactory) : base(dataContextFactory)
        {
        }


        public async Task<IReadOnlyCollection<Slide>> GetAll()
        {
            using (var dc = DataContext())
            {
                return await dc.Slides.ToListAsync();
            }
        }

        public async Task<Slide> Add(Slide slide)
        {
            using (var dc = DataContext())
            {
                dc.Slides.Add(slide);
                await dc.SaveChangesAsync();
                return slide;
            }
        }

        public async Task<Slide> GetById(int id)
        {
            using (var dc = DataContext())
            {
                return await dc.Slides.SingleOrDefaultAsync(i => i.SlideId == id);
            }
        }

        public async Task<Slide> Edit(Slide slide)
        {
            using (var dc = DataContext())
            {
                dc.SetModified(slide);
                await dc.SaveChangesAsync();
                return slide;
            }
        }

        public async Task Remove(Slide slide)
        {
            using (var dc = DataContext())
            {
                dc.Slides.Attach(slide);
                dc.Slides.Remove(slide);
                await dc.SaveChangesAsync();
            }
        }
    }
}
