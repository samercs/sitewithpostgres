using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using SiteWithPostgres.Data;
using SiteWithPostgres.Entities;

namespace SiteWithPostgres.Services
{
    public class ImageService : ServiceBase
    {
        public ImageService(IDataContextFactory dataContextFactory) : base(dataContextFactory)
        {
        }

        public async Task<IReadOnlyCollection<Image>> GetAll()
        {
            using (var dc = DataContext())
            {
                return await dc.Images.ToListAsync();
            }
        }

        public async Task<Image> Add(Image image)
        {
            using (var dc = DataContext())
            {
                dc.Images.Add(image);
                await dc.SaveChangesAsync();
                return image;
            }
        }

        public async Task<Image> GetById(int id)
        {
            using (var dc = DataContext())
            {
                return await dc.Images.SingleOrDefaultAsync(i => i.ImageId == id);
            }
        }

        public async Task<Image> Edit(Image img)
        {
            using (var dc = DataContext())
            {
                dc.SetModified(img);
                await dc.SaveChangesAsync();
                return img;
            }
        }

        public async Task Remove(Image img)
        {
            using (var dc = DataContext())
            {
                dc.Images.Attach(img);
                dc.Images.Remove(img);
                await dc.SaveChangesAsync();
            }
        }

        public async Task<Image> GetByTag(string tag)
        {
            using (var dc = DataContext())
            {
                return await dc.Images.FirstOrDefaultAsync(i =>
                    i.ImageKey.Equals(tag, StringComparison.InvariantCultureIgnoreCase));
            }
        }
    }
}
