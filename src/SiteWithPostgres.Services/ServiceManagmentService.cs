using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using SiteWithPostgres.Data;
using SiteWithPostgres.Entities;

namespace SiteWithPostgres.Services
{
    public class ServiceManagmentService: ServiceBase
    {
        public ServiceManagmentService(IDataContextFactory dataContextFactory) : base(dataContextFactory)
        {
        }

        public async Task<IReadOnlyCollection<Service>> GetAll()
        {
            using (var dc = DataContext())
            {
                return await dc.Services.ToListAsync();
            }
        }

        public async Task<Service> Add(Service service)
        {
            using (var dc = DataContext())
            {
                dc.Services.Add(service);
                await dc.SaveChangesAsync();
                return service;
            }
        }

        public async Task<Service> GetById(int id)
        {
            using (var dc = DataContext())
            {
                return await dc.Services.SingleOrDefaultAsync(i => i.ServiceId == id);
            }
        }

        public async Task<Service> Edit(Service service)
        {
            using (var dc = DataContext())
            {
                dc.SetModified(service);
                await dc.SaveChangesAsync();
                return service;
            }
        }

        public async Task Remove(Service service)
        {
            using (var dc = DataContext())
            {
                dc.Services.Attach(service);
                dc.Services.Remove(service);
                await dc.SaveChangesAsync();
            }
        }
    }
}
