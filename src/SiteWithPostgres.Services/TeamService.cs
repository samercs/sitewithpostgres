using SiteWithPostgres.Data;
using SiteWithPostgres.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace SiteWithPostgres.Services
{
    public class TeamService : ServiceBase
    {
        public TeamService(IDataContextFactory dataContextFactory) : base(dataContextFactory)
        {
        }

        public async Task<IReadOnlyCollection<Team>> GetAll()
        {
            using (var dc = DataContext())
            {
                return await dc.Teams.ToListAsync();
            }
        }

        public async Task<Team> Add(Team team)
        {
            using (var dc = DataContext())
            {
                dc.Teams.Add(team);
                await dc.SaveChangesAsync();
                return team;
            }
        }

        public async Task<Team> GetById(int id)
        {
            using (var dc = DataContext())
            {
                return await dc.Teams.SingleOrDefaultAsync(i => i.TeamId == id);
            }
        }

        public async Task<Team> Edit(Team team)
        {
            using (var dc = DataContext())
            {
                dc.SetModified(team);
                await dc.SaveChangesAsync();
                return team;
            }
        }

        public async Task Remove(Team team)
        {
            using (var dc = DataContext())
            {
                dc.Teams.Attach(team);
                dc.Teams.Remove(team);
                await dc.SaveChangesAsync();
            }
        }
    }
}
