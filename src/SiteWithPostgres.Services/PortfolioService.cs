using SiteWithPostgres.Data;
using SiteWithPostgres.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace SiteWithPostgres.Services
{
    public class PortfolioService : ServiceBase
    {
        public PortfolioService(IDataContextFactory dataContextFactory) : base(dataContextFactory)
        {
        }

        public async Task<IReadOnlyCollection<Portfolio>> GetAll()
        {
            using (var dc = DataContext())
            {
                return await dc.Portfolios.Include(i => i.Types).ToListAsync();
            }
        }

        public async Task<Portfolio> Add(Portfolio portfolio)
        {
            using (var dc = DataContext())
            {
                dc.Portfolios.Add(portfolio);
                await dc.SaveChangesAsync();
                return portfolio;
            }
        }

        public async Task<Portfolio> GetById(int id)
        {
            using (var dc = DataContext())
            {
                return await dc.Portfolios.SingleOrDefaultAsync(i => i.PortfolioId == id);
            }
        }

        public async Task<Portfolio> Edit(Portfolio portfolio)
        {
            using (var dc = DataContext())
            {
                dc.SetModified(portfolio);
                await dc.SaveChangesAsync();
                return portfolio;
            }
        }

        public async Task Remove(Portfolio portfolio)
        {
            using (var dc = DataContext())
            {
                dc.Portfolios.Attach(portfolio);
                dc.Portfolios.Remove(portfolio);
                await dc.SaveChangesAsync();
            }
        }

        public async Task<IReadOnlyCollection<PortfolioType>> GetAllTypes()
        {
            using (var dc = DataContext())
            {
                return await dc.PortfolioTypes.ToListAsync();
            }
        }
    }
}
