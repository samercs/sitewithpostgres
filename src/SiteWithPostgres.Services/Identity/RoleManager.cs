using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SiteWithPostgres.Data;

namespace SiteWithPostgres.Services.Identity
{
    public class RoleManager : RoleManager<IdentityRole>
    {
        public RoleManager(IRoleStore<IdentityRole, string> store) : base(store)
        {
        }

        public RoleManager(IDataContextFactory dataContextFactory)
            : base(new RoleStore<IdentityRole>((DbContext) dataContextFactory.GetContext()))
        {       
        }
    }
}
