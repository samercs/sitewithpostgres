using SiteWithPostgres.Data;
using SiteWithPostgres.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace SiteWithPostgres.Services
{
    public class StatisticService : ServiceBase
    {
        public StatisticService(IDataContextFactory dataContextFactory) : base(dataContextFactory)
        {
        }

        public async Task<IReadOnlyCollection<Statistic>> GetAll()
        {
            using (var dc = DataContext())
            {
                return await dc.Statistics.ToListAsync();
            }
        }

        public async Task<Statistic> Add(Statistic statistic)
        {
            using (var dc = DataContext())
            {
                dc.Statistics.Add(statistic);
                await dc.SaveChangesAsync();
                return statistic;
            }
        }

        public async Task<Statistic> GetById(int id)
        {
            using (var dc = DataContext())
            {
                return await dc.Statistics.SingleOrDefaultAsync(i => i.StatisticId == id);
            }
        }

        public async Task<Statistic> Edit(Statistic statistic)
        {
            using (var dc = DataContext())
            {
                dc.SetModified(statistic);
                await dc.SaveChangesAsync();
                return statistic;
            }
        }

        public async Task Remove(Statistic statistic)
        {
            using (var dc = DataContext())
            {
                dc.Statistics.Attach(statistic);
                dc.Statistics.Remove(statistic);
                await dc.SaveChangesAsync();
            }
        }
    }
}
