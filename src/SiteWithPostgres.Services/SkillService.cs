using SiteWithPostgres.Data;
using SiteWithPostgres.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace SiteWithPostgres.Services
{
    public class SkillService : ServiceBase
    {
        public SkillService(IDataContextFactory dataContextFactory) : base(dataContextFactory)
        {
        }

        public async Task<IReadOnlyCollection<Skill>> GetAll()
        {
            using (var dc = DataContext())
            {
                return await dc.Skills.ToListAsync();
            }
        }

        public async Task<Skill> Add(Skill skill)
        {
            using (var dc = DataContext())
            {
                dc.Skills.Add(skill);
                await dc.SaveChangesAsync();
                return skill;
            }
        }

        public async Task<Skill> GetById(int id)
        {
            using (var dc = DataContext())
            {
                return await dc.Skills.SingleOrDefaultAsync(i => i.SkillId == id);
            }
        }

        public async Task<Skill> Edit(Skill skill)
        {
            using (var dc = DataContext())
            {
                dc.SetModified(skill);
                await dc.SaveChangesAsync();
                return skill;
            }
        }

        public async Task Remove(Skill skill)
        {
            using (var dc = DataContext())
            {
                dc.Skills.Attach(skill);
                dc.Skills.Remove(skill);
                await dc.SaveChangesAsync();
            }
        }
    }
}
