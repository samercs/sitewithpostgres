using SiteWithPostgres.Data;
using SiteWithPostgres.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace SiteWithPostgres.Services
{
    public class ResourceService : ServiceBase
    {
        public ResourceService(IDataContextFactory dataContextFactory) : base(dataContextFactory)
        {
        }

        public async Task<IReadOnlyCollection<Resource>> GetAll()
        {
            using (var dc = DataContext())
            {
                return await dc.Resources.ToListAsync();
            }
        }

        public async Task<Resource> Add(Resource resources)
        {
            using (var dc = DataContext())
            {
                dc.Resources.Add(resources);
                await dc.SaveChangesAsync();
                return resources;
            }
        }

        public async Task<Resource> GetById(int id)
        {
            using (var dc = DataContext())
            {
                return await dc.Resources.SingleOrDefaultAsync(i => i.ResourceId == id);
            }
        }

        public async Task<Resource> Edit(Resource resource)
        {
            using (var dc = DataContext())
            {
                dc.SetModified(resource);
                await dc.SaveChangesAsync();
                return resource;
            }
        }

        public async Task Remove(Resource resources)
        {
            using (var dc = DataContext())
            {
                dc.Resources.Attach(resources);
                dc.Resources.Remove(resources);
                await dc.SaveChangesAsync();
            }
        }
    }
}
