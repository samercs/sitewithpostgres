using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using SiteWithPostgres.Data;
using SiteWithPostgres.Entities;

namespace SiteWithPostgres.Services
{
    public class PageService : ServiceBase
    {
        public PageService(IDataContextFactory dataContextFactory) : base(dataContextFactory)
        {
        }

        public async Task<IReadOnlyCollection<Page>> GetAll()
        {
            using (var dc = DataContext())
            {
                return await dc.Pages.ToListAsync();
            }
        }

        public async Task<Page> Add(Page page)
        {
            using (var dc = DataContext())
            {
                dc.Pages.Add(page);
                await dc.SaveChangesAsync();
                return page;
            }
        }

        public async Task<Page> GetById(int id)
        {
            using (var dc = DataContext())
            {
                return await dc.Pages.SingleOrDefaultAsync(i => i.PageId == id);
            }
        }

        public async Task<Page> Edit(Page page)
        {
            using (var dc = DataContext())
            {
                dc.SetModified(page);
                await dc.SaveChangesAsync();
                return page;
            }
        }

        public async Task Remove(Page page)
        {
            using (var dc = DataContext())
            {
                dc.Pages.Attach(page);
                dc.Pages.Remove(page);
                await dc.SaveChangesAsync();
            }
        }

        public async Task<Page> GetByTag(string tagName)
        {
            using (var dc = DataContext())
            {
                return await dc.Pages.FirstOrDefaultAsync(i =>
                    i.TagName.Equals(tagName, StringComparison.InvariantCultureIgnoreCase));
            }
        }
    }
}
