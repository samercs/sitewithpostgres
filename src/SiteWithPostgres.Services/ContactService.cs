using SiteWithPostgres.Data;
using SiteWithPostgres.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace SiteWithPostgres.Services
{
    public class ContactService : ServiceBase
    {
        public ContactService(IDataContextFactory dataContextFactory) : base(dataContextFactory)
        {
        }

        public async Task<IReadOnlyCollection<Contact>> GetAll()
        {
            using (var dc = DataContext())
            {
                return await dc.Contacts.ToListAsync();
            }
        }

        public async Task<Contact> Add(Contact contact)
        {
            using (var dc = DataContext())
            {
                dc.Contacts.Add(contact);
                await dc.SaveChangesAsync();
                return contact;
            }
        }

        public async Task<Contact> GetById(int id)
        {
            using (var dc = DataContext())
            {
                return await dc.Contacts.SingleOrDefaultAsync(i => i.Id == id);
            }
        }

        public async Task<Contact> Edit(Contact contact)
        {
            using (var dc = DataContext())
            {
                dc.SetModified(contact);
                await dc.SaveChangesAsync();
                return contact;
            }
        }

        public async Task Remove(Contact contact)
        {
            using (var dc = DataContext())
            {
                dc.Contacts.Attach(contact);
                dc.Contacts.Remove(contact);
                await dc.SaveChangesAsync();
            }
        }
    }
}
