using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using SiteWithPostgres.Data;
using SiteWithPostgres.Entities;

namespace SiteWithPostgres.Services
{
    public class LinkService : ServiceBase
    {
        public LinkService(IDataContextFactory dataContextFactory) : base(dataContextFactory)
        {
        }

        public async Task<IReadOnlyCollection<Link>> GetAll()
        {
            using (var dc = DataContext())
            {
                return await dc.Links.ToListAsync();
            }
        }

        public async Task<Link> Add(Link link)
        {
            using (var dc = DataContext())
            {
                dc.Links.Add(link);
                await dc.SaveChangesAsync();
                return link;
            }
        }

        public async Task<Link> GetById(int id)
        {
            using (var dc = DataContext())
            {
                return await dc.Links.SingleOrDefaultAsync(i => i.LinkId == id);
            }
        }

        public async Task<Link> Edit(Link link)
        {
            using (var dc = DataContext())
            {
                dc.SetModified(link);
                await dc.SaveChangesAsync();
                return link;
            }
        }

        public async Task Remove(Link link)
        {
            using (var dc = DataContext())
            {
                dc.Links.Attach(link);
                dc.Links.Remove(link);
                await dc.SaveChangesAsync();
            }
        }

    }
}
