using OrangeJetpack.Localization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteWithPostgres.Entities
{
    public class Resource : ModelBase, ILocalizable
    {
        public int ResourceId { get; set; }

        [Required]
        [Index(IsUnique = true)]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [Localized]
        public string Value { get; set; }


    }
}
