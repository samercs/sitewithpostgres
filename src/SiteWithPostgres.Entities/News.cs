using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using OrangeJetpack.Localization;

namespace SiteWithPostgres.Entities
{
    [Serializable]
    public class News: ModelBase, ILocalizable
    {
        public int NewsId { get; set; }
        [Required, Localized]
        public string Title { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [AllowHtml]
        [Localized]
        [UIHint("TextArea")]
        public string Content { get; set; }

        public int DisplayOrder { get; set; }

    }
}
