using OrangeJetpack.Localization;
using System.ComponentModel.DataAnnotations;

namespace SiteWithPostgres.Entities
{
    public class Statistic : ModelBase, ILocalizable
    {
        public int StatisticId { get; set; }
        [Required]
        [Localized]
        public string Title { get; set; }
        [Required]
        [Localized]
        public string Value { get; set; }
        [Localized]
        public string Prev { get; set; }

    }
}
