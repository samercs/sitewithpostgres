using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteWithPostgres.Entities
{
    public class PortfolioTypePortfolio
    {
        [Key, Column(Order = 1)]
        public int PortfolioId { get; set; }

        [Key, Column(Order = 2)]
        public int PortfolioTypeId { get; set; }

        [ForeignKey("PortfolioId")]
        public virtual Portfolio Portfolio { get; set; }
        [ForeignKey("PortfolioTypeId")]
        public virtual PortfolioType PortfolioType { get; set; }
    }
}
