using SiteWithPostgres.Entities.Enum;
using OrangeJetpack.Localization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteWithPostgres.Entities
{
    public class Image : ModelBase, ILocalizable
    {
        public int ImageId { get; set; }
        [Required]
        public string Url { get; set; }
        [Required]
        [Localized]
        public string Title { get; set; }
        [DefaultValue(ItemStatus.Public)]
        public ItemStatus Status { get; set; }

        [Required]
        [Display(Name = "Image Key")]
        [Index(IsUnique = true)]
        [StringLength(100)]
        public string ImageKey { get; set; }

    }
}
