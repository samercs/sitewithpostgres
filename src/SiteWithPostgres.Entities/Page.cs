using OrangeJetpack.Localization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;


namespace SiteWithPostgres.Entities
{
    public class Page : ModelBase, ILocalizable
    {
        public int PageId { get; set; }
        [Required]
        [Localized]
        public string Title { get; set; }
        [Required, StringLength(100), Index("IX_Tag", IsUnique = true)]
        public string TagName { get; set; }
        [UIHint("TextArea")]
        [AllowHtml]
        [Localized]
        public string Content { get; set; }
        [Localized]
        [UIHint("TextArea")]
        public string Preview { get; set; }

    }
}
