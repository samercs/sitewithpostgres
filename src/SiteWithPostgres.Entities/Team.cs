using System.ComponentModel.DataAnnotations;
using OrangeJetpack.Localization;

namespace SiteWithPostgres.Entities
{
    public class Team: ModelBase, ILocalizable
    {
        public int TeamId { get; set; }
        [Required]
        [Localized]
        public string Name { get; set; }
        [Required]
        [Localized]
        public string JobTitle { get; set; }
        public string Image { get; set; }
        [Localized]
        public string Prev { get; set; }
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        [DataType(DataType.Url)]
        public string Facebook { get; set; }
        [DataType(DataType.Url)]
        public string Twitter { get; set; }
        public string PersonalWebsite { get; set; }
    }
}
