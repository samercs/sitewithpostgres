using System.ComponentModel.DataAnnotations;

namespace SiteWithPostgres.Entities
{
    public class Contact : ModelBase
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Phone { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        public string Text { get; set; }

        public bool? IsRead { get; set; }
    }
}
