namespace SiteWithPostgres.Entities.Enum
{
    public enum ItemStatus
    {
        Public = 1,
        Private = 2
    }
}
