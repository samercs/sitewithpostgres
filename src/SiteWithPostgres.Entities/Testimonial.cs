using System.ComponentModel.DataAnnotations;

namespace SiteWithPostgres.Entities
{
    public class Testimonial: ModelBase
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Job { get; set; }
        [Required]
        public string Txt { get; set; }
    }
}
