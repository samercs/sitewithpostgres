using OrangeJetpack.Localization;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SiteWithPostgres.Entities
{
    public class PortfolioType : ModelBase, ILocalizable
    {
        public int PortfolioTypeId { get; set; }
        [Required]
        [Localized]
        public string Title { get; set; }

        public virtual ICollection<Portfolio> Portfolios { get; set; }
    }
}
