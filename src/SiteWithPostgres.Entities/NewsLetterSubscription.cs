using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteWithPostgres.Entities
{
    public class NewsLetterSubscription: ModelBase
    {
        public int Id { get; set; }
        [DataType(DataType.EmailAddress)]
        [Required]
        [Index("IX_Email", IsUnique = true)]
        [StringLength(100)]
        public string Email { get; set; }
    }
}
