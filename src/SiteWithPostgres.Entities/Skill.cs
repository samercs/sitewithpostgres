using OrangeJetpack.Localization;
using System.ComponentModel.DataAnnotations;

namespace SiteWithPostgres.Entities
{
    public class Skill : ModelBase, ILocalizable
    {
        public int SkillId { get; set; }
        [Required]
        [Localized]
        public string Title { get; set; }
        [Required]
        [Range(0, 100)]
        public int Rate { get; set; }
    }
}
