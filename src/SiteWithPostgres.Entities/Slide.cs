using OrangeJetpack.Localization;
using System.ComponentModel.DataAnnotations;
using SiteWithPostgres.Entities.Enum;

namespace SiteWithPostgres.Entities
{
    public class Slide : ModelBase, ILocalizable
    {
        public int SlideId { get; set; }
        [Required]
        public string Url { get; set; }
        [Localized]
        public string Title { get; set; }

        [Required]
        public int DisplayOrder { get; set; }

        [Display(Name = "Line 2")]
        [Localized]
        public string Title2 { get; set; }

        [Display(Name = "Line 3")]
        [Localized]
        public string Title3 { get; set; }

        [Required]
        [Display(Name = "Visible")]
        public ItemStatus ItemStatus { get; set; }

        [DataType(DataType.Url)]
        public string NavigationUrl { get; set; }


    }
}
