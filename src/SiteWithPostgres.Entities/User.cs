using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using OrangeJetpack.Base.Core.Formatting;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SiteWithPostgres.Entities
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [Required, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedUtc { get; set; }

        [Required, StringLength(2)]
        public string LanguageCode { get; set; }

        [StringLength(4, MinimumLength = 1)]
        public string PhoneCountryCode { get; set; }

        [StringLength(15, MinimumLength = 7)]
        [Column("PhoneNumber")]
        public string PhoneLocalNumber { get; set; }

        public new string PhoneNumber => PhoneFormatter.Format(PhoneCountryCode, PhoneLocalNumber);

        public int? AddressId { get; set; }

        public virtual Address Address { get; set; }

        public string Image { get; set; }

        public int CityId { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            return await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
