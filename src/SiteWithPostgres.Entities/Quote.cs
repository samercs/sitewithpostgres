using OrangeJetpack.Localization;
using System.ComponentModel.DataAnnotations;

namespace SiteWithPostgres.Entities
{
    public class Quote : ModelBase, ILocalizable
    {
        public int QuoteId { get; set; }
        [Required]
        [Localized]
        public string Title { get; set; }

        [Localized]
        public string Name { get; set; }
    }
}
