using OrangeJetpack.Localization;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SiteWithPostgres.Entities
{
    public class Portfolio : ModelBase, ILocalizable
    {
        public int PortfolioId { get; set; }
        [Localized]
        public string Title { get; set; }
        public string Image { get; set; }
        [Localized]
        public string Name { get; set; }
        [DataType(DataType.Url)]
        public string NavigationUrl { get; set; }

        public virtual ICollection<PortfolioType> Types { get; set; }

    }
}
