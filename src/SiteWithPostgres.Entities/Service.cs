using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using OrangeJetpack.Localization;

namespace SiteWithPostgres.Entities
{
    public class Service: ModelBase, ILocalizable
    {
        public int ServiceId { get; set; }
        [Localized]
        [Required]
        public string Title { get; set; }
        [Localized]
        [AllowHtml]
        [UIHint("TextArea")]
        public string Description { get; set; }

        [Localized]
        public string Prev { get; set; }

        public string Image { get; set; }

    }
}
