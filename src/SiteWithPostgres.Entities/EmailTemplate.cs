using OrangeJetpack.Localization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SiteWithPostgres.Core.Enums;

namespace SiteWithPostgres.Entities
{
    public class EmailTemplate : ModelBase, ILocalizable
    {
        public int EmailTemplateId { get; set; }

        [Index(IsUnique = true)]
        public EmailTemplateType TemplateType { get; set; }

        [Required]

        public string Description { get; set; }

        [Required, Localized, Display(Name = "Subject")]
        public string Subject { get; set; }

        [Required, Localized, Display(Name = "Message")]
        [UIHint("TextArea")]
        public string Message { get; set; }
    }
}
