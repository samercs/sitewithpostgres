using OrangeJetpack.Localization;
using System.ComponentModel.DataAnnotations;

namespace SiteWithPostgres.Entities
{
    public class Link : ModelBase, ILocalizable
    {
        public int LinkId { get; set; }
        [Required]
        [Localized]
        public string Title { get; set; }

        [DataType(DataType.Url)]
        [Required]
        public string Url { get; set; }
    }
}
