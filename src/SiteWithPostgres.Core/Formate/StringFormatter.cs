using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace SiteWithPostgres.Core.Formate
{
    public class StringFormatter
    {
        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static readonly Regex NotDigitsRegex = new Regex(@"[^(0-9|/\u0660-\u0669/)]", RegexOptions.Compiled);

        /// <summary>
        /// Gets a string with all non-numeric digits removed.
        /// </summary>
        public static string StripNonDigits(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            return NotDigitsRegex.Replace(input, "").Trim();
        }

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static readonly Regex HtmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Gets a string with all HTML tags removed.
        /// </summary>
        public static string StripHtmlTags(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            return HtmlRegex.Replace(input, "");
        }

        /// <summary>
        /// Compiled regular expressions for performance.
        /// </summary>
        static readonly Regex LocalizationRegex1 = new Regex(@"^\[\{", RegexOptions.Compiled);
        static readonly Regex LocalizationRegex2 = new Regex(@"""?}]$", RegexOptions.Compiled);
        static readonly Regex LocalizationRegex3 = new Regex(@"""k"":""[a-z]{2}"",""v"":""", RegexOptions.Compiled);
        static readonly Regex LocalizationRegex4 = new Regex(@"""},\{", RegexOptions.Compiled);
        static readonly Regex MultipleSpacesRegex = new Regex(@"\s\s+", RegexOptions.Compiled);
        static readonly Regex SpecialCharactersRegex = new Regex(@"[~!@#$%^&*()|+=?;:,.<>\{\}\[\]\\\/]", RegexOptions.Compiled);

        /// <summary>
        /// Gets a string with localization JSON removed.
        /// </summary>
        public static string StripLocalizationJson(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            input = LocalizationRegex1.Replace(input, "");
            input = LocalizationRegex2.Replace(input, "");
            input = LocalizationRegex3.Replace(input, "");
            input = LocalizationRegex4.Replace(input, " ");

            return input;
        }

        public static string FormatCamelCase(object camelCasedString)
        {
            var returnValue = camelCasedString.ToString();

            // Strip leading "_" character
            returnValue = Regex.Replace(returnValue, "^_", "").Trim();
            // Add a space between each lower case character and upper case character
            returnValue = Regex.Replace(returnValue, "([a-z])([A-Z])", "$1 $2").Trim();
            // Add a space between 2 upper case characters when the second one is followed by a lower space character
            returnValue = Regex.Replace(returnValue, "([A-Z])([A-Z][a-z])", "$1 $2").Trim();

            return returnValue;
        }

        /// <summary>
        /// Gets a string with MS Word HTML removed.
        /// </summary>
        public static string StripWordHtml(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            input = StripHtmlComments(input);
            input = StripXmlTags(input);
            input = StripWordTags(input);
            input = StripEmptyTags(input);

            return input;
        }

        public static string StripSpecialCharacters(string input)
        {
            return string.IsNullOrEmpty(input) ? input : ConsolidateMultipleSpaces(SpecialCharactersRegex.Replace(input, ""));
        }

        private static string ConsolidateMultipleSpaces(string input)
        {
            return MultipleSpacesRegex.Replace(input, " ").Trim();
        }

        private static string StripHtmlComments(string input)
        {
            var regex = new Regex(@"\<!--.*?-->", RegexOptions.Singleline);
            return regex.Replace(input, "");
        }

        private static string StripXmlTags(string input)
        {
            var regex = new Regex(@"<xml>.*?</xml>", RegexOptions.Singleline);
            return regex.Replace(input, "");
        }

        private static string StripWordTags(string input)
        {
            var regex = new Regex(@"<([a-z]{1}:\w+)>.*?</(\1)>", RegexOptions.Singleline);
            return regex.Replace(input, "");
        }

        private static string StripEmptyTags(string input)
        {
            var regex = new Regex(@"<(\w+)></(\1)>", RegexOptions.Singleline);
            return regex.Replace(input, "");
        }

        /// <summary>
        /// Gets a string.Format() equivalent using an object as the formatting source.
        /// </summary>
        /// <example>
        /// String.Formatter.HaackFormat("Hello {FirstName}", new { FirstName = "Andy" });
        /// </example>
        /// <remarks>http://haacked.com/archive/2009/01/14/named-formats-redux.aspx/</remarks>
        public static string ObjectFormat(string format, object source)
        {
            if (format == null)
            {
                throw new ArgumentNullException("format");
            }
            var formattedStrings = (from expression in SplitFormat(format)
                                    select expression.Eval(source)).ToArray();
            return String.Join("", formattedStrings);
        }

        private static IEnumerable<ITextExpression> SplitFormat(string format)
        {
            int exprEndIndex = -1;
            int expStartIndex;

            do
            {
                expStartIndex = IndexOfExpressionStart(format, exprEndIndex + 1);
                if (expStartIndex < 0)
                {
                    //everything after last end brace index.
                    if (exprEndIndex + 1 < format.Length)
                    {
                        yield return new LiteralFormat(
                            format.Substring(exprEndIndex + 1));
                    }
                    break;
                }

                if (expStartIndex - exprEndIndex - 1 > 0)
                {
                    //everything up to next start brace index
                    yield return new LiteralFormat(format.Substring(exprEndIndex + 1
                      , expStartIndex - exprEndIndex - 1));
                }

                int endBraceIndex = IndexOfExpressionEnd(format, expStartIndex + 1);
                if (endBraceIndex < 0)
                {
                    //rest of string, no end brace (could be invalid expression)
                    yield return new FormatExpression(format.Substring(expStartIndex));
                }
                else
                {
                    exprEndIndex = endBraceIndex;
                    //everything from start to end brace.
                    yield return new FormatExpression(format.Substring(expStartIndex
                      , endBraceIndex - expStartIndex + 1));

                }
            } while (expStartIndex > -1);
        }

        static int IndexOfExpressionStart(string format, int startIndex)
        {
            int index = format.IndexOf('{', startIndex);
            if (index == -1)
            {
                return index;
            }

            //peek ahead.
            if (index + 1 < format.Length)
            {
                char nextChar = format[index + 1];
                if (nextChar == '{')
                {
                    return IndexOfExpressionStart(format, index + 2);
                }
            }

            return index;
        }

        static int IndexOfExpressionEnd(string format, int startIndex)
        {
            int endBraceIndex = format.IndexOf('}', startIndex);
            if (endBraceIndex == -1)
            {
                return endBraceIndex;
            }
            //start peeking ahead until there are no more braces...
            // }}}}
            int braceCount = 0;
            for (int i = endBraceIndex + 1; i < format.Length; i++)
            {
                if (format[i] == '}')
                {
                    braceCount++;
                }
                else
                {
                    break;
                }
            }
            if (braceCount % 2 == 1)
            {
                return IndexOfExpressionEnd(format, endBraceIndex + braceCount + 1);
            }

            return endBraceIndex;
        }
    }
}
