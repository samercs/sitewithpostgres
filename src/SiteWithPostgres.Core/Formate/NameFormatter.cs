namespace SiteWithPostgres.Core.Formate
{
    public class NameFormatter
    {
        public static string Formate(string fname, string lname)
        {
            return $"{fname} {lname}";
        }

        public static string GetLastNameCommaFirstName(string fname, string lname)
        {
            return $"{lname}, {fname}";
        }
    }
}
