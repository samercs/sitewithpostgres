namespace SiteWithPostgres.Core.Formate
{
    public interface ITextExpression
    {
        string Eval(object o);
    }
}
