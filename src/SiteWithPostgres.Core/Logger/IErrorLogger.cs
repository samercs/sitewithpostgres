using System;

namespace SiteWithPostgres.Core.Logger
{
    public interface IErrorLogger
    {
        void Log(Exception ex);
        void Log(string message);
    }
}
