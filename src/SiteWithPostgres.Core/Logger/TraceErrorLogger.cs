using System;
using System.Diagnostics;

namespace SiteWithPostgres.Core.Logger
{
    public class TraceErrorLogger : IErrorLogger
    {
        public void Log(Exception ex)
        {
            Trace.TraceError(ex.Message);
        }

        public void Log(string message)
        {
            Trace.TraceError(message);
        }
    }
}
