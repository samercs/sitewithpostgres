using System;

namespace SiteWithPostgres.Core.Providers
{
    public interface ITimeProvider
    {
        DateTime Now();
    }
}
