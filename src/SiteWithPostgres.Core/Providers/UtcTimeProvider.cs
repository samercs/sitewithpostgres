using System;

namespace SiteWithPostgres.Core.Providers
{
    public class UtcTimeProvider : ITimeProvider
    {
        public DateTime Now()
        {
            return DateTime.UtcNow;
        }
    }
}
