
namespace SiteWithPostgres.Core.Enums
{
    public enum EmailTemplateType
    {
        AccountRegistration = 1,
        PasswordReset = 2,
        PasswordChanged = 3,
        EmailChanged = 4,
        NoAccount = 5
    }
}
