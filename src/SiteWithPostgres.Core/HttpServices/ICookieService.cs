using System;

namespace SiteWithPostgres.Core.HttpServices
{
    public interface ICookieService
    {
        string Get(string key);
        int? TryGetInt(string key, int? defaultValue = null);
        bool TryGetBool(string key, bool defaultValue = false);
        void Add(string key, object value, DateTime? expiration = null, string path = null, bool httpOnly = true);
        void Add(string key, string value, DateTime? expiration = null, string path = null, bool httpOnly = true);
    }
}
