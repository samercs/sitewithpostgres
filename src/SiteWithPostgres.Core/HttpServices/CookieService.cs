using System;
using System.Web;

namespace SiteWithPostgres.Core.HttpServices
{
    public class CookieService : ICookieService
    {
        private readonly HttpRequestBase _httpRequestBase;
        private readonly HttpResponseBase _httpResponseBase;

        public CookieService(HttpContextBase httpContext)
        {
            _httpRequestBase = httpContext.Request;
            _httpResponseBase = httpContext.Response;
        }

        public string Get(string key)
        {
            return _httpRequestBase.Cookies[key] == null ? null : _httpRequestBase.Cookies[key].Value;
        }

        public int? TryGetInt(string key, int? defaultValue = null)
        {
            if (_httpRequestBase.Cookies[key] == null)
            {
                return defaultValue;
            }

            int output;
            return int.TryParse(_httpRequestBase.Cookies[key].Value, out output) ? (int?)output : defaultValue;
        }

        public bool TryGetBool(string key, bool defaultValue = false)
        {
            if (_httpRequestBase.Cookies[key] == null)
            {
                return defaultValue;
            }

            bool output;
            return bool.TryParse(_httpRequestBase.Cookies[key].Value, out output) ? output : defaultValue;
        }

        public void Add(string key, object value, DateTime? expiration = null, string path = null, bool httpOnly = true)
        {
            Add(key, value.ToString(), expiration, path, httpOnly);
        }

        public void Add(string key, string value, DateTime? expiration = null, string path = null, bool httpOnly = true)
        {
            var cookie = new HttpCookie(key)
            {
                HttpOnly = httpOnly,
                Value = value
            };

            if (expiration.HasValue)
            {
                if (expiration.Value.Equals(DateTime.MaxValue))
                {
                    expiration = DateTime.Today.AddYears(10);
                }

                cookie.Expires = expiration.Value;
            }

            if (!string.IsNullOrEmpty(path))
            {
                cookie.Path = path;
            }

            _httpResponseBase.AppendCookie(cookie);
        }
    }
}
