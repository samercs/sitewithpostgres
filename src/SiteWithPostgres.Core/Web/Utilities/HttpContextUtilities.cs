using System.Web;

namespace SiteWithPostgres.Core.Web.Utilities
{
    public static class HttpContextUtilities
    {
        /// <summary>
        /// Removes the ASP.NET headers from all HTTP responses.
        /// </summary>
        /// <remarks>For example, removes X-AspNet-Version and X-AspNetMvc-Version.</remarks>
        public static void RemoveAspNetHeaders(HttpResponse response)
        {
            response.Headers.Remove("Server");
            response.Headers.Remove("X-AspNet-Version");
            response.Headers.Remove("X-AspNetMvc-Version");
        }
    }
}
