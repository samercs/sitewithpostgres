using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SiteWithPostgres.Core.Web.Utilities
{
    internal class GenericController : Controller { }

    public abstract class ControllerUtilities
    {
        public static string GetPartialViewAsString(string viewName, object model = null)
        {
            var controller = CreateController();

            if (string.IsNullOrEmpty(viewName))
            {
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");
            }

            if (model != null)
            {
                controller.ViewData.Model = model;
            }

            return RenderView(controller.ControllerContext, controller.ViewData, viewName);
        }

        private static GenericController CreateController()
        {
            var wrapper = new HttpContextWrapper(HttpContext.Current);

            var routeData = new RouteData();
            routeData.Values.Add("controller", "generic");

            var controller = new GenericController();
            controller.ControllerContext = new ControllerContext(wrapper, routeData, controller);

            return controller;
        }

        private static string RenderView(ControllerContext controllerContext, ViewDataDictionary viewData, string viewName)
        {
            using (var sw = new StringWriter())
            {
                var viewResult = new RazorViewEngine().FindPartialView(controllerContext, viewName, false);
                var viewContext = new ViewContext(controllerContext, viewResult.View, viewData, new TempDataDictionary(), sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}
