using SiteWithPostgres.Core.Email;
using System.Threading.Tasks;

namespace SiteWithPostgres.Core.Service
{
    public interface IMessageService
    {
        Task<EmailResponse> Send(Email.Email email);
        Task<SmsResponse> Send(Sms sms);
        Task<SlackResponse> Send(Slack slack);
    }
}
