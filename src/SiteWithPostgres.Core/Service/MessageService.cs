using SiteWithPostgres.Core.Email;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SiteWithPostgres.Core.Service
{
    public class MessageService : IMessageService
    {
        private readonly string _senderAddress;
        private readonly string _senderName;

        public MessageService(string projectKey, string projectToken, string senderAddress = null, string senderName = null)
        {

            _senderAddress = senderAddress;
            _senderName = senderName;
        }

        public MessageService(EmailSettings emailSettings)
        {
            _senderAddress = emailSettings.SenderAddress;
            _senderName = emailSettings.SenderName;
        }

        /// <summary>
        /// Sends an email message using a third-party email service provide such as SendGrid.
        /// </summary>
        public async Task<EmailResponse> Send(Email.Email email)
        {
            email.FromAddress = email.FromAddress ?? _senderAddress;
            email.FromName = email.FromName ?? _senderName;

            var fromAddress = new MailAddress(email.FromAddress, email.FromName);
            var toAddress = new MailAddress(email.ToAddress);
            const string fromPassword = "traing!@#";
            string body = email.Message;

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = email.Subject,
                Body = body,
                IsBodyHtml = true
            })
            {
                smtp.Send(message);
            }


            var response = new EmailResponse()
            {
                SendDate = DateTime.Now,
                IsSuccess = true
            };

            return await Task.FromResult(response);
        }

        /// <summary>
        /// Sends an SMS message using a third-party SMS service provider such as Twilio.
        /// </summary>
        public async Task<SmsResponse> Send(Sms sms)
        {
            //var response = await _httpClient.PostAsJsonAsync("messages/sms", sms);

            return new SmsResponse
            {
                IsSuccess = true
            };
        }

        /// <summary>
        /// Sends a Slack message
        /// </summary>
        /// <param name="slack"></param>
        /// <returns></returns>
        public async Task<SlackResponse> Send(Slack slack)
        {
            //var response = await _httpClient.PostAsJsonAsync("messages/slack", slack);

            return new SlackResponse
            {
                IsSuccess = true
            };
        }
    }
}
