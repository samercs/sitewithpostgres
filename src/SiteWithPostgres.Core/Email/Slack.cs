using System;

namespace SiteWithPostgres.Core.Email
{
    [Serializable]
    public class Slack
    {
        public Uri Uri { get; set; }

        public string Channel { get; set; }

        public string UserName { get; set; }

        public string Text { get; set; }
    }
}
