namespace SiteWithPostgres.Core.Email
{
    public abstract class ResponseBase
    {
        public bool IsSuccess { get; set; }
    }
}
