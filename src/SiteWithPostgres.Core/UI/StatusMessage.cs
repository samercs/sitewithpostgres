using System.Collections.Generic;
using System.Linq;

namespace SiteWithPostgres.Core.UI
{
    public class StatusMessage
    {
        private readonly Dictionary<StatusMessageType, string> _messageTypeClasses = new Dictionary<StatusMessageType, string>
        {
            { StatusMessageType.Success, "alert-success" },
            { StatusMessageType.Information, "alert-info" },
            { StatusMessageType.Warning, "alert-warning" },
            { StatusMessageType.Error, "alert-danger" }
        };

        private readonly Dictionary<StatusMessageFormat, string> _messageFormatClasses = new Dictionary<StatusMessageFormat, string>
        {
            { StatusMessageFormat.Normal, "" },
            { StatusMessageFormat.Block, "alert-block" }
        };

        public string Message { get; set; }
        public StatusMessageType StatusMessageType { get; set; }
        public StatusMessageFormat StatusMessageFormat { get; set; }

        public StatusMessage(string message,
            StatusMessageType statusMessageType = StatusMessageType.Success,
            StatusMessageFormat statusMessageFormat = StatusMessageFormat.Normal,
            bool includeIcon = true)
        {
            Message = includeIcon ? AddIcon(message, statusMessageType) : message;
            StatusMessageType = statusMessageType;
            StatusMessageFormat = statusMessageFormat;
        }

        public string GetCssClass()
        {
            var cssClasses = new List<string> { "alert" };

            var messageTypeClass = _messageTypeClasses[StatusMessageType];
            if (!string.IsNullOrEmpty(messageTypeClass))
            {
                cssClasses.Add(messageTypeClass);
            }

            var messageFormatClass = _messageFormatClasses[StatusMessageFormat];
            if (!string.IsNullOrEmpty(messageFormatClass))
            {
                cssClasses.Add(messageFormatClass);
            }

            return string.Join(" ", cssClasses.Where(i => !string.IsNullOrEmpty(i)));
        }

        private static string AddIcon(string message, StatusMessageType statusMessageType)
        {
            switch (statusMessageType)
            {
                case StatusMessageType.Success:
                    message = "<i class='fa fa-check-square fa-2x pull-left'></i> " + message;
                    break;
                case StatusMessageType.Information:
                    message = "<i class='fa fa-question-circle fa-2x pull-left'></i> " + message;
                    break;
                case StatusMessageType.Warning:
                    message = "<i class='fa fa-warning fa-2x pull-left'></i> " + message;
                    break;
                case StatusMessageType.Error:
                    message = "<i class='fa fa-exclamation-triangle fa-2x pull-left'></i> " + message;
                    break;
            }
            return message;
        }
    }
}
