namespace SiteWithPostgres.Core.UI
{
    public enum StatusMessageType
    {
        Success,
        Information,
        Warning,
        Error
    }
}
