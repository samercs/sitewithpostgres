using System;

namespace SiteWithPostgres.Core.Security.Exceptions
{
    public class UrlTokenException : Exception { }
}
