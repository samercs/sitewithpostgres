

namespace SiteWithPostgres.Core.Security.Exceptions
{
    public class ExpiredTimestampException : UrlTokenException { }
}
