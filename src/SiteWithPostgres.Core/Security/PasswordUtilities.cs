using System;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
namespace SiteWithPostgres.Core.Security
{
    public class PasswordUtilities
    {
        private const string Salt = "ZpWj6zk3qGNbkxeDPQuD";
        private const int ResetPasswordExpiration = 2880; // 48 hours

        /// <summary>
        /// Generates a random 8 digit password.
        /// </summary>
        public static string GenerateRandomPassword()
        {
            const string chars = "ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz23456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }

        /// <summary>
        /// Generates a URL with the appropriate querystring and token to validate reset.
        /// </summary>
        public static string GenerateResetPasswordUrl(string resetPasswordUrl, string email)
        {
            var timestamp = DateTime.UtcNow.Ticks.ToString(CultureInfo.InvariantCulture);
            var input = string.Join("|", new[] { email, timestamp, Salt });
            var hash = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(input));
            var token = HttpUtility.UrlEncode(Convert.ToBase64String(hash));

            var uriBuilder = new UriBuilder(resetPasswordUrl)
            {
                Query = $"Email={email}&Timestamp={timestamp}&Token={token}"
            };

            return uriBuilder.Uri.ToString();
        }

        /// <summary>
        /// Validates URL querystring parameters required for trusting a password reset and return true.
        /// </summary>
        public static bool ValidateResetPasswordParameters(UrlTokenParameters parameters)
        {
            if (!ValidateParameters(parameters))
            {
                throw new MissingParametersException();
            }

            if (!ValidateTimestamp(parameters, ResetPasswordExpiration))
            {
                throw new ExpiredTimestampException();
            }

            if (!ValidateToken(parameters))
            {
                throw new InvalidTokenException();
            }

            return true;
        }

        private static bool ValidateParameters(UrlTokenParameters parameters)
        {
            return new[] { parameters.Email, parameters.Timestamp, parameters.Token }.All(param => !string.IsNullOrEmpty(param));
        }

        private static bool ValidateTimestamp(UrlTokenParameters parameters, int expiration)
        {
            try
            {
                var timestamp = new DateTime(Convert.ToInt64(parameters.Timestamp));
                return DateTime.Now.Subtract(timestamp).TotalMinutes <= expiration;
            }
            catch
            {
                return false;
            }
        }

        private static bool ValidateToken(UrlTokenParameters parameters)
        {
            var urlDecode = HttpUtility.UrlDecode(parameters.Token);
            if (urlDecode == null)
            {
                return false;
            }

            var token = urlDecode.Replace(" ", "+");
            var input = string.Join("|", parameters.Email, parameters.Timestamp, Salt);
            var hash = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(input));

            return Convert.ToBase64String(hash).Equals(token);
        }
    }

    public class InvalidTokenException : UrlTokenException { }

    public class ExpiredTimestampException : UrlTokenException { }

    public class MissingParametersException : UrlTokenException { }

    public class UrlTokenException : Exception { }
}
