using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SiteWithPostgres.Web.Areas.User.Models;
using SiteWithPostgres.Web.Core.Identity;

namespace SiteWithPostgres.Web.Models.Account
{
    public class NeedPasswordViewModel
    {
        [Required]
        [StringLength(256)]
        [EmailAddress]
        public string Email { get; set; }
    }
    
    public class SignInViewModel
    {
        [Required]
        [StringLength(256)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password), AllowHtml]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
        
        [Required]
        [StringLength(256)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(int.MaxValue, MinimumLength = 8)]
        [DataType(DataType.Password), AllowHtml]
        public string Password { get; set; }

        [Required, StringLength(4, MinimumLength = 1)]
        public string PhoneCountryCode { get; set; }

        [Required, StringLength(15, MinimumLength = 7)]
        public string PhoneLocalNumber { get; set; }

        public PhoneNumberViewModel PhoneNumber { get; set; }

        //public AddressViewModel Address { get; set; }

        public Role? ValidateRoleToken(Role? role, string token)
        {
            if (role == Role.Administrator && token.Equals("ITfdpPnVBeLy6W2cxNk5agOgrJJqzLK7BThcQSYxaiZUPigXBaTy5zXfwO4X"))
            {
                return role.Value;
            }
            return null;
        }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [StringLength(int.MaxValue, MinimumLength = 8)]
        [DataType(DataType.Password), AllowHtml]
        public string NewPassword { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
