
namespace SiteWithPostgres.Web.Models.Shared
{
    public class ConfirmationViewModel
    {
        public string PageTitle { get; set; }
        public string ConfirmationMessage { get; set; }
    }
}
