using SiteWithPostgres.Entities;

namespace SiteWithPostgres.Web.Models.Shared
{
    public class PageViewModel
    {
        public Page Page { get; set; }
        public Image Image { get; set; }
    }
}
