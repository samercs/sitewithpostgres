using SiteWithPostgres.Entities;

namespace SiteWithPostgres.Web.Models.Shared
{
    public class ResourceViewModel
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public static ResourceViewModel Create(Resource resource)
        {
            return new ResourceViewModel()
            {
                Value = resource.Value,
                Name = resource.Name
            };
        }
    }
}
