using System.Collections.Generic;
using SiteWithPostgres.Entities;

namespace SiteWithPostgres.Web.Models.Shared
{
    public class NewsListViewModel
    {
        public IEnumerable<News> Newses { get; set; }
        public IEnumerable<Testimonial> Testimonials { get; set; }
    }
}
