using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SiteWithPostgres.Core.Regionalization;
using SiteWithPostgres.Entities;

namespace SiteWithPostgres.Web.Models.Shared
{
    public class AddressViewModel : Address
    {
        public IEnumerable<SelectListItem> CountriesList { get; set; }

        public AddressViewModel()
        {

        }

        public AddressViewModel(Address address)
        {
            AddressId = address.AddressId;
            AddressLine1 = address.AddressLine1;
            AddressLine2 = address.AddressLine2;
            AddressLine3 = address.AddressLine3;
            AddressLine4 = address.AddressLine4;
            CityArea = address.CityArea;
            StateProvince = address.StateProvince;
            PostalCode = address.PostalCode;
            CountryCode = address.CountryCode;
        }

        public AddressViewModel Hydrate()
        {
            CountriesList = Countries.GetAllCountries()
                .Select(i => new SelectListItem
                {
                    Text = i.Name,
                    Value = i.IsoCode,
                    Selected = i.IsoCode.Equals(CountryCode)
                })
                .ToList();

            return this;
        }

        public IEnumerable<string> KuwaitAreas
        {
            get
            {
                return new[]
                {
                    "���� �����",
                    "��� ��������",
                    "��� �����",
                    "��� �����",
                    "������",
                    "�������",
                    "�������",
                    "�������",
                    "�����",
                    "��������",
                    "�������",
                    "�������",
                    "��������",
                    "������",
                    "������",
                    "������",
                    "������",
                    "�������",
                    "������",
                    "�����",
                    "������",
                    "��������",
                    "������",
                    "���� ",
                    "�������",
                    "��������",
                    "�����",
                    "������",
                    "�������",
                    "�����",
                    "�����",
                    "�������",
                    "������",
                    "��������",
                    "��������",
                    "������",
                    "���������",
                    "��������",
                    "������",
                    "�����",
                    "��������",
                    "�������� ��������",
                    "�������",
                    "������",
                    "��������",
                    "�������",
                    "�������",
                    "������",
                    "��������",
                    "�������",
                    "���������",
                    "���������",
                    "�������",
                    "��������",
                    "�������",
                    "������",
                    "������",
                    "�����",
                    "������",
                    "�������� - ���� ������",
                    "�������",
                    "�������",
                    "������ ",
                    "���������",
                    "������",
                    "��������",
                    "������",
                    "������",
                    "������",
                    "������",
                    "������",
                    "������",
                    "������",
                    "������ �������",
                    "�������",
                    "���� �����",
                    "����",
                    "�����",
                    "���� ������",
                    "���� �����",
                    "���� ������",
                    "���� ������",
                    "����",
                    "����",
                    "����",
                    "�����",
                    "�����",
                    "��� ���������",
                    "����",
                    "���� ������ �������",
                    "���� ������",
                    "���� ������",
                    "������",
                    "������� ������",
                    "������� ������� - ��� ������",
                    "��� ���� ������",
                    "������",
                    "��� ������",
                    "������",
                    "�����",
                    "�����",
                    "����� ���������",
                    "����� ������",
                    "����� ������",
                    "����� ������",
                    "�����",
                    "����",
                    "����� ����",
                    "Abbasiya",
                    "Abdullah Al-Mubarak - West Jleeb",
                    "Abdullah Al-Salem",
                    "Abraq Khaitan",
                    "Abu Ftaira",
                    "Abu Halifa",
                    "Abu Hasaniya",
                    "Adailiya",
                    "Adan",
                    "Ahmadi",
                    "Al Dasmah",
                    "Al Masayel",
                    "Al Naeem",
                    "Al-Ahmadi",
                    "Al-Bedae",
                    "Al-Qurain",
                    "Al-Qusour",
                    "Ali Sabah Al-Salem",
                    "Andalous",
                    "Ardhiya",
                    "Ardhiya Industrial",
                    "Ashbeliah",
                    "Bayan",
                    "Bnaid Al-Gar",
                    "Daiya",
                    "Dasma",
                    "Dasman",
                    "Dhaher",
                    "Dhajeej",
                    "Doha",
                    "Egaila",
                    "Fahad Al Ahmed",
                    "Fahaheel",
                    "Faiha",
                    "Farwaniya",
                    "Farwaniya",
                    "Ferdous",
                    "Fintas",
                    "Fnaitees",
                    "Ghornata",
                    "Hadiya",
                    "Hawally",
                    "Hawally",
                    "Hitteen",
                    "Jaber Al Ahmed",
                    "Jaber Al-Ali",
                    "Jabriya",
                    "Jahra",
                    "Jahra",
                    "Jleeb Al-Shiyoukh",
                    "Kaifan",
                    "Khaitan",
                    "Khaldiya",
                    "Kuwait City",
                    "Kuwait City (Capital)",
                    "Magwa",
                    "Mahboula",
                    "Maidan Hawally",
                    "Mangaf",
                    "Mansouriya",
                    "Messila",
                    "Mirqab",
                    "Mishrif",
                    "Mubarak Al-Abdullah",
                    "Mubarak Al-kabir",
                    "Mubarak Al-Kabir",
                    "Nahda",
                    "Nasseem",
                    "Nuzha",
                    "Omariya",
                    "Oyoun",
                    "Qadsiya",
                    "Qairawan - South Doha",
                    "Qasr",
                    "Qibla",
                    "Qortuba",
                    "Rabiya",
                    "Rai",
                    "Rawda",
                    "Reggai",
                    "Rehab",
                    "Riqqa",
                    "Rumaithiya",
                    "Saad Al Abdullah",
                    "Sabah AL Ahmad Residential",
                    "Sabah Al-Nasser",
                    "Sabah Al-Salem",
                    "Sabahiya",
                    "Salam",
                    "Salhiya",
                    "Salmiya",
                    "Salwa",
                    "Shaab",
                    "Shamiya",
                    "Sharq",
                    "Shuhada",
                    "Shuwaikh",
                    "Siddiq",
                    "South Wista",
                    "Sulaibikhat",
                    "Sulaibiya",
                    "Surra",
                    "Taima",
                    "Wafra Residential",
                    "Waha",
                    "Wista",
                    "Yarmouk",
                    "Zahra"
                };
            }
        }
    }
}
