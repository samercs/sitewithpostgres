using System.Collections.Generic;
using SiteWithPostgres.Entities;

namespace SiteWithPostgres.Web.Models.Home
{
    public class HomeImagesViewModel
    {
        public IReadOnlyCollection<Image> Images { get; set; }
    }
}
