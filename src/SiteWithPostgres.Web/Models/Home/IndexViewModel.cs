using SiteWithPostgres.Entities;
using SiteWithPostgres.Web.Models.Shared;
using System.Collections.Generic;

namespace SiteWithPostgres.Web.Models.Home
{
    public class IndexViewModel
    {
        public IReadOnlyCollection<Service> Services { get; set; }
        public PageViewModel WelcomePage { get; set; }
        public Page SkillPage { get; set; }
        public IReadOnlyCollection<Slide> Slides { get; set; }
        public IReadOnlyCollection<Team> Teams { get; set; }
        public IReadOnlyCollection<Skill> Skills { get; set; }
        public IReadOnlyCollection<Statistic> Statistics { get; set; }
        public IReadOnlyCollection<Quote> Quotes { get; set; }
        public IReadOnlyCollection<PortfolioType> Types { get; set; }
        public IReadOnlyCollection<Portfolio> Portfolios { get; set; }
    }
}
