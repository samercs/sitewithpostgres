using SiteWithPostgres.Core.Email;
using SiteWithPostgres.Core.Web.Utilities;
namespace SiteWithPostgres.Web.Extensions
{
    public static class ControllerExtensions
    {
        public static Email WithTemplate(this Email email)
        {
            var messageWithTemplate = ControllerUtilities.GetPartialViewAsString("EmailTemplate", email);
            email.Message = messageWithTemplate;

            return email;
        }
    }
}
