using System.Collections.Specialized;
using SiteWithPostgres.Core.Service;

namespace SiteWithPostgres.Web.Core.Configuration
{
    public class AppSettings : IAppSettings
    {
        public string SiteTitle { get; }
        public string ProjectKey { get; }
        public string ProjectToken { get; }
        public string ApiKey { get; }
        public string Url { get; }
        public EmailSettings EmailSettings { get; }
        
        public AppSettings(NameValueCollection appSettings)
        {
            SiteTitle = appSettings["SiteTitle"];
            ProjectKey = appSettings["OrangeJetpack.Services:Project.Key"];
            ProjectToken = appSettings["OrangeJetpack.Services:Project.Token"];
            ApiKey = appSettings["ApiKey"];
            Url = appSettings["url"];

            EmailSettings = new EmailSettings
            {
                ProjectKey = ProjectKey,
                ProjectToken = ProjectToken,
                SenderAddress = appSettings["OrangeJetpack.Services:Email.Sender"],
                SenderName = SiteTitle
            };
        }
    }
}
