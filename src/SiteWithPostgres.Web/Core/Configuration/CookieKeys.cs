namespace SiteWithPostgres.Web.Core.Configuration
{
    public static class CookieKeys
    {
        public static string LanguageCode = "LanguageCode";
        public static string LastSignInEmail = "LastSignInEmail";
        public static string DisplayName = "DisplayName";
    }
}
