using SiteWithPostgres.Core.Formate;
using SiteWithPostgres.Web.Core.Services;
using System.Configuration;
using System.Web.Mvc;

namespace SiteWithPostgres.Web.Core.Configuration
{
    public abstract class CustomViewPage : WebViewPage
    {
        private IAppSettings _appSettings;
        public IAppSettings AppSettings
        {
            get { return (_appSettings = _appSettings ?? new AppSettings(ConfigurationManager.AppSettings)); }
        }

        public string LanguageCode
        {
            get { return ViewBag.LanguageCode ?? "en"; }
        }

        /// <summary>
        /// Gets a formatting string from OrangeJetpack.Base.Core.Formatting.StringFormatter.ObjectFormat();
        /// </summary>
        public MvcHtmlString F(string format, object source)
        {
            return MvcHtmlString.Create(StringFormatter.ObjectFormat(format, source));
        }
    }

    public abstract class CustomViewPage<TModel> : WebViewPage<TModel>
    {
        private IAppSettings _appSettings;

        public IAppSettings AppSettings
        {
            get { return (_appSettings = _appSettings ?? new AppSettings(ConfigurationManager.AppSettings)); }
        }

        public string LanguageCode
        {
            get { return ViewBag.LanguageCode ?? "en"; }
        }

        public ILanguageService LanguageService => DependencyResolver.Current.GetService<ILanguageService>();

        /// <summary>
        /// Gets a formatting string from OrangeJetpack.Base.Core.Formatting.StringFormatter.ObjectFormat();
        /// </summary>
        public MvcHtmlString F(string format, object source)
        {
            return MvcHtmlString.Create(StringFormatter.ObjectFormat(format, source));
        }
    }
}
