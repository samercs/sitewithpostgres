using SiteWithPostgres.Core.Service;

namespace SiteWithPostgres.Web.Core.Configuration
{
    public interface IAppSettings
    {
        string SiteTitle { get; }
        string ProjectKey { get; }
        string ProjectToken { get; }
        string ApiKey { get; }
        string Url { get; }
        EmailSettings EmailSettings { get; }
    }
}
