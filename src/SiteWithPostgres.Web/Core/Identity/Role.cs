
namespace SiteWithPostgres.Web.Core.Identity
{
    public enum Role
    {
        Administrator,
        Representative,
        TourGuide,
        User
    }
}
