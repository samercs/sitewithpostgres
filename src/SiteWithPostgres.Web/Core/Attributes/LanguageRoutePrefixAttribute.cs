using System.Web.Mvc;

namespace SiteWithPostgres.Web.Core.Attributes
{
    public class LanguageRoutePrefixAttribute : RoutePrefixAttribute
    {
        private readonly string _area;

        public LanguageRoutePrefixAttribute(string prefix) : base(prefix)
        {

        }

        public LanguageRoutePrefixAttribute(string area, string prefix) : base(prefix)
        {
            _area = area;
        }

        public override string Prefix
        {
            get
            {
                var prefix = "{languageCode:regex(^en|ar$)}";

                if (string.IsNullOrWhiteSpace(base.Prefix))
                {
                    return prefix;
                }

                if (!string.IsNullOrWhiteSpace(_area))
                {
                    prefix += "/" + _area;
                }

                prefix = prefix + "/" + base.Prefix;

                return prefix;
            }
        }
    }
}
