using SiteWithPostgres.Core.Logger;
using SiteWithPostgres.Core.Service;
using SiteWithPostgres.Data;
using SiteWithPostgres.Web.Core.Configuration;

namespace SiteWithPostgres.Web.Core.Services
{
    public interface IAppServices
    {
        IDataContextFactory DataContextFactory { get; }
        IAppSettings AppSettings { get; }
        IAuthService AuthService { get; }
        ICookieService CookieService { get; }
        IMessageService MessageService { get; }
        IErrorLogger ErrorLogger { get; }

        ILanguageService LanguageService { get; }
    }
}
