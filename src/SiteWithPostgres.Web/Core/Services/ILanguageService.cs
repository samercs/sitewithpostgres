namespace SiteWithPostgres.Web.Core.Services
{
    public interface ILanguageService
    {
        string GetResourceByLangKey(string lang, string key);
        void LoadResourceFromServer();

    }
}
