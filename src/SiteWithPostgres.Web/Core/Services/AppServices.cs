using SiteWithPostgres.Core.Logger;
using SiteWithPostgres.Core.Service;
using SiteWithPostgres.Data;
using SiteWithPostgres.Web.Core.Configuration;

namespace SiteWithPostgres.Web.Core.Services
{
    public class AppServices : IAppServices
    {
        public IDataContextFactory DataContextFactory { get; }
        public IAppSettings AppSettings { get; }
        public IAuthService AuthService { get; }
        public ICookieService CookieService { get; }
        public IMessageService MessageService { get; }
        public IErrorLogger ErrorLogger { get; }

        public ILanguageService LanguageService { get; }

        public AppServices(
            IDataContextFactory dataContextFactory,
            IAppSettings appSettings,
            IAuthService authService,
            ICookieService cookieService,
            IMessageService messageService,
             IErrorLogger errorLogger,
             ILanguageService languageService)
        {
            DataContextFactory = dataContextFactory;
            AppSettings = appSettings;
            AuthService = authService;
            CookieService = cookieService;
            MessageService = messageService;
            ErrorLogger = errorLogger;
            LanguageService = languageService;
        }
    }
}
