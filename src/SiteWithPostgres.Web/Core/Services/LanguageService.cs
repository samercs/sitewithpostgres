using SiteWithPostgres.Data;
using SiteWithPostgres.Entities;
using SiteWithPostgres.Services;
using SiteWithPostgres.Web.Core.Config;
using SiteWithPostgres.Web.Models.Shared;
using OrangeJetpack.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace SiteWithPostgres.Web.Core.Services
{
    public class LanguageService : ILanguageService
    {
        private readonly HttpContextBase _context;
        private IList<ResourceViewModel> _resourcesCashed;
        private readonly ResourceService _resourceService;

        public LanguageService(HttpContextBase httpContext, IDataContextFactory contextFactory)
        {
            _context = httpContext;
            _resourcesCashed = (IList<ResourceViewModel>)_context.Cache[CachConstants.ResourceCash];
            _resourceService = new ResourceService(contextFactory);
        }

        public string GetResourceByLangKey(string lang, string key)
        {
            if (_resourcesCashed != null)
            {
                var resourceValue = _resourcesCashed.FirstOrDefault(i =>
                    i.Name.Equals(key, StringComparison.InvariantCultureIgnoreCase));
                if (resourceValue != null)
                {
                    var localizedContent = LocalizedContent.Deserialize(resourceValue.Value);
                    return localizedContent.FirstOrDefault(i =>
                            i.Key.Equals(lang, StringComparison.InvariantCultureIgnoreCase))
                        ?.Value;
                }
                else
                {
                    return $"--({key})--";
                }
            }
            LoadResourceFromServer();
            return GetResourceByLangKey(lang, key);
        }

        public void LoadResourceFromServer()
        {
            Task<IReadOnlyCollection<Resource>> task = Task.Run(() => _resourceService.GetAll());
            var databaseResources = task.Result;
            var data = databaseResources.Select(ResourceViewModel.Create);
            _context.Cache[CachConstants.ResourceCash] = data.ToList();
            _resourcesCashed = data.ToList();
        }
    }
}
