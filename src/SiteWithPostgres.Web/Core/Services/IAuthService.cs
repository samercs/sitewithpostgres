using Microsoft.AspNet.Identity;
using System;
using System.Security.Principal;
using System.Threading.Tasks;
using SiteWithPostgres.Entities;

namespace SiteWithPostgres.Web.Core.Services
{
    public interface IAuthService
    {
        bool IsAuthenticated();
        bool IsLocal();
        string CurrentUserId();
        string AnonymousId();
        string UserHostAddress();

        Task<User> SignIn(string email, string password, bool rememberMe);
        Task<User> SignIn(User user);
        void LogOut();

        Task<User> CurrentUser();
        
        Task<IdentityResult> CreateUser(User user, string password, Func<User, Task> onSuccess = null);
        Task<IdentityResult> ChangePassword(IIdentity identity, string oldPassword, string newPassword);

    }
}
