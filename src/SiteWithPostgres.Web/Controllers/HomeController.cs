using SiteWithPostgres.Entities;
using SiteWithPostgres.Services;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Configuration;
using SiteWithPostgres.Web.Core.Services;
using SiteWithPostgres.Web.Models.Home;
using SiteWithPostgres.Web.Models.Shared;
using OrangeJetpack.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SiteWithPostgres.Web.Controllers
{
    [LanguageRoutePrefix("")]
    public class HomeController : ApplicationController
    {
        private readonly ServiceManagmentService _managmentService;
        private readonly PageService _pageService;
        private readonly ImageService _imageService;
        private readonly ContactService _contactService;
        private readonly SlideService _slideService;
        private readonly LinkService _linkService;
        private readonly TeamService _teamService;
        private readonly SkillService _skillService;
        private readonly StatisticService _statisticService;
        private readonly QuoteService _quoteService;
        private readonly PortfolioService _portfolioService;
        public HomeController(IAppServices appServices) : base(appServices)
        {
            _managmentService = new ServiceManagmentService(DataContextFactory);
            _pageService = new PageService(DataContextFactory);
            _imageService = new ImageService(DataContextFactory);
            _contactService = new ContactService(DataContextFactory);
            _slideService = new SlideService(DataContextFactory);
            _linkService = new LinkService(DataContextFactory);
            _teamService = new TeamService(DataContextFactory);
            _skillService = new SkillService(DataContextFactory);
            _statisticService = new StatisticService(DataContextFactory);
            _quoteService = new QuoteService(DataContextFactory);
            _portfolioService = new PortfolioService(DataContextFactory);
        }

        [Route("~/")]
        public ActionResult IndexWithLanguage()
        {
            return RedirectToAction("Index", new { LanguageCode });
        }

        [Route("")]
        public async Task<ActionResult> Index()
        {
            var services = await _managmentService.GetAll();
            var welcomePage = await _pageService.GetByTag("welcome");
            var skillPage = await _pageService.GetByTag("OurSkill");
            var welcomeImage = await _imageService.GetByTag("welcome");
            var slides = await _slideService.GetAll();
            var teams = await _teamService.GetAll();
            var skilss = await _skillService.GetAll();
            var statistics = await _statisticService.GetAll();
            slides = slides.OrderBy(i => i.DisplayOrder).Localize(LanguageCode, i => i.Title, i => i.Title2, i => i.Title3).ToList();
            var quotes = await _quoteService.GetAll();
            var types = await _portfolioService.GetAllTypes();
            var portfolios = await _portfolioService.GetAll();
            //foreach (var portfolio in portfolios)
            //{
            //    portfolio.Types =  portfolio.Types.Localize(LanguageCode, i => i.Title);
            //}
            var model = new IndexViewModel()
            {
                Services = services.Localize(LanguageCode, i => i.Title, i => i.Prev).ToList(),
                WelcomePage = new PageViewModel()
                {
                    Page = welcomePage?.Localize(LanguageCode, i => i.Title, i => i.Content) ?? new Page(),
                    Image = welcomeImage ?? new Image()
                },
                Slides = slides.OrderBy(i => i.DisplayOrder).ToList(),
                Teams = teams.Localize(LanguageCode, i => i.Name, i => i.JobTitle, i => i.Prev).ToList(),
                Skills = skilss.Localize(LanguageCode, i => i.Title).ToList(),
                SkillPage = skillPage?.Localize(LanguageCode, i => i.Content, i => i.Title) ?? new Page(),
                Statistics = statistics.Localize(LanguageCode, i => i.Title, i => i.Value, i => i.Prev).ToList(),
                Quotes = quotes.Localize(LanguageCode, i => i.Title, i => i.Name).ToList(),
                Types = types.Localize(LanguageCode, i => i.Title).ToList(),
                Portfolios = portfolios.Localize(LanguageCode, i => i.Title, i => i.Name).ToList()

            };
            return View(model);
        }

        [Route("about")]
        public async Task<ActionResult> About()
        {
            var about = await _pageService.GetByTag("about");
            if (about == null)
            {
                return HttpNotFound();
            }
            var image = await _imageService.GetByTag("about");
            var model = new PageViewModel()
            {
                Page = about.Localize(LanguageCode, i => i.Title, i => i.Content),
                Image = image
            };


            return View(model);
        }

        [Route("contact")]
        public ActionResult Contact()
        {
            var contact = new Contact();
            return View(contact);
        }

        [Route("contact")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Contact(Contact contact)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(contact);
                }

                await _contactService.Add(contact);
                SetStatusMessage("Thank you for contact SiteWithPostgres our team will response to your message soon.");
                return RedirectToAction("Contact");
            }
            catch
            {
                return View(contact);
            }

        }

        [Route("terms-and-conditions")]
        public ActionResult TermsAndConditions()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        [Route("~/change-language")]
        public async Task<ActionResult> ChangeLanguage(string language, Uri referrer)
        {
            CookieService.Add(CookieKeys.LanguageCode, language, DateTime.Today.AddYears(10));

            if (Request.IsAuthenticated)
            {
                var user = await AuthService.CurrentUser();
                user.LanguageCode = language;
                await UserService.UpdateUser(user);
            }

            var redirectUrl = Regex.Replace(referrer.ToString(), @"/(en|ar)", "/" + language);
            return Redirect(redirectUrl);
        }

        [Route("~/ok")]
        public ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public ActionResult FooterLocation()
        {
            Page page = new Page();
            Task.Run(async () =>
            {
                page = await _pageService.GetByTag("LocationFooter");
            }).Wait();
            if (page == null)
            {
                page = new Page();
            }

            return PartialView(page.Localize(LanguageCode, i => i.Title, i => i.Content));
        }

        public ActionResult StayTuned()
        {
            Page page = new Page();
            Task.Run(async () =>
            {
                page = await _pageService.GetByTag("StayTuned");
            }).Wait();
            if (page == null)
            {
                page = new Page();
            }

            return PartialView(page.Localize(LanguageCode, i => i.Title, i => i.Content));
        }

        public ActionResult Links()
        {
            IReadOnlyCollection<Link> model = new List<Link>();
            Task.Run(async () =>
            {
                model = await _linkService.GetAll();
            }).Wait();

            return PartialView(model.Localize(LanguageCode, i => i.Title));
        }


    }
}
