using SiteWithPostgres.Core.Enums;
using SiteWithPostgres.Entities;
using SiteWithPostgres.Services.Identity;
using SiteWithPostgres.Web.Areas.User.Models;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Configuration;
using SiteWithPostgres.Web.Core.Services;
using SiteWithPostgres.Web.Extensions;
using SiteWithPostgres.Web.Models.Account;
using SiteWithPostgres.Web.Models.Shared;
using SiteWithPostgres.Web.Resources;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using SiteWithPostgres.Core.Email;
using SiteWithPostgres.Core.Formate;
using SiteWithPostgres.Core.Security;
using SiteWithPostgres.Core.UI;

namespace SiteWithPostgres.Web.Controllers
{
    [LanguageRoutePrefix("", "account")]
    public class AccountController : ApplicationController
    {
        private readonly UserService _userService;

        public AccountController(IAppServices appServices) : base(appServices)
        {
            _userService = new UserService(DataContextFactory);
        }

        [Route("log-in")]
        public ActionResult Login(string returnUrl, int? shipmentId)
        {
            var viewModel = new SignInViewModel
            {
                ReturnUrl = returnUrl,
                Email = CookieService.Get(CookieKeys.LastSignInEmail)
            };

            return View(viewModel);
        }

        [Route("log-in")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(SignInViewModel model, string returnUrl, int? shipmentId)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await AuthService.SignIn(model.Email, model.Password, model.RememberMe);
            if (user == null)
            {
                SetStatusMessage(Global.InvalidUserNameOrPassword, StatusMessageType.Error);
                return View(model);
            }

            SetNameAndEmailCookies(user, model.Email);

            return !string.IsNullOrEmpty(returnUrl)
                ? RedirectToLocal(returnUrl)
                : RedirectToAction("MyAccount", "Account", new { Area = "User", LanguageCode });
        }

        private void SetNameAndEmailCookies(User user, string usernameOrEmail)
        {
            CookieService.Add(CookieKeys.DisplayName, user.FirstName, DateTime.Today.AddYears(10));
            CookieService.Add(CookieKeys.LastSignInEmail, usernameOrEmail, DateTime.Today.AddYears(10));
        }

        [Route("log-out")]
       public ActionResult LogOut()
        {
            AuthService.LogOut();
            return RedirectToAction("Index", "Home", new { LanguageCode });
        }

        [Route("sign-up")]
        public ActionResult Register()
        {
            var countryCode = Request.Headers.Get("HTTP_CF_IPCountry");
            var viewModel = new RegisterViewModel
            {
                PhoneNumber = new PhoneNumberViewModel
                {
                    CountryCode = string.IsNullOrWhiteSpace(countryCode) ? "jo" : countryCode
                }
            };

            return View(viewModel);
        }

        [Route("change-country")]
        public PartialViewResult ChangeCountry(string countryCode)
        {
            var viewName = AddressPartialResolver.GetViewName(countryCode);
            var viewModel = new AddressViewModel();

            ViewData.TemplateInfo = new TemplateInfo
            {
                HtmlFieldPrefix = "Address"
            };

            return PartialView(viewName, viewModel);
        }

        [Route("sign-up")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = new User
            {
                UserName = model.Email,
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                LanguageCode = LanguageCode,
                PhoneCountryCode = "+" + StringFormatter.StripNonDigits(model.PhoneCountryCode),
                PhoneLocalNumber = StringFormatter.StripNonDigits(model.PhoneLocalNumber)
            };

            var result = await AuthService.CreateUser(user, model.Password);
            if (!result.Succeeded)
            {
                SetStatusMessage(string.Format(Global.RegistrationErrorMessage));
                return View(model);
            }

            await SendNewUserWelcomeEmail(user);
            SetNameAndEmailCookies(user, user.Email);

            SetStatusMessage(StringFormatter.ObjectFormat(Global.RegistrationWelcomeMessage, new
            {
                AppSettings.SiteTitle,
                user.FirstName
            }));

            return RedirectToAction("Index", "Home");
        }

        private async Task SendNewUserWelcomeEmail(User user)
        {
            var template = await EmailTemplateService.GetByTemplateType(EmailTemplateType.AccountRegistration, LanguageCode);
            var email = new Email
            {
                ToAddress = user.Email,
                Subject = StringFormatter.ObjectFormat(template.Subject, new { AppSettings.SiteTitle }),
                Message = StringFormatter.ObjectFormat(template.Message, new { AppSettings.SiteTitle, user.FirstName })
            };

            try
            {
                await MessageService.Send(email.WithTemplate());
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
            }
        }

        [Route("need-password")]
        public ActionResult NeedPassword()
        {
            var viewModel = new NeedPasswordViewModel();

            return View(viewModel);
        }

        [Route("need-password")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> NeedPassword(NeedPasswordViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(new NeedPasswordViewModel());
            }

            await SendPasswordResetNotification(viewModel.Email);

            SetStatusMessage(string.Format(Global.ResetPasswordEmailSentAcknowledgement, viewModel.Email));

            return RedirectToAction("Login", new { LanguageCode });
        }

        private async Task SendPasswordResetNotification(string emailAddress)
        {
            EmailTemplate template;
            var email = new Email
            {
                ToAddress = emailAddress,
                Subject = Global.ResetPassword
            };

            var user = await _userService.GetUserByEmail(emailAddress);
            if (user == null)
            {
                template = await EmailTemplateService.GetByTemplateType(EmailTemplateType.NoAccount, LanguageCode);
                email.Subject = template.Subject;
                email.Message = StringFormatter.ObjectFormat(template.Message, new { emailAddress });
            }
            else
            {
                template = await EmailTemplateService.GetByTemplateType(EmailTemplateType.PasswordReset, LanguageCode);
                email.Subject = template.Subject;
                email.Message = StringFormatter.ObjectFormat(template.Message, new
                {
                    user.FirstName,
                    PasswordResetUrl = GetPasswordResetUrl(user.Email)
                });
            }

            await MessageService.Send(email.WithTemplate());
        }

        [Route("reset-password")]
        public ActionResult ResetPassword(UrlTokenParameters tokenParameters)
        {
            string errorMessage;
            if (!ValidateTokenParameters(tokenParameters, out errorMessage))
            {
                return StatusMessage(errorMessage, StatusMessageType.Error);
            }

            return View(new ResetPasswordViewModel());
        }

        [Route("reset-password")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(UrlTokenParameters tokenParameters, ResetPasswordViewModel viewModel)
        {
            string errorMessage;
            if (!ValidateTokenParameters(tokenParameters, out errorMessage))
            {
                return Error(errorMessage);
            }

            var user = await _userService.GetUserByEmail(tokenParameters.Email);
            if (user == null)
            {
                return Error(Global.ResetPasswordCannotFindUserAccount);
            }

            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            user.PasswordHash = _userService.HashPassword(viewModel.NewPassword);
            await _userService.UpdateUser(user);
            await AuthService.SignIn(user);

            SetStatusMessage(Global.PasswordSuccessfullyChanged);

            return RedirectToAction("MyAccount", "Account", new { Area = "User", LanguageCode });
        }

        private string GetBaseUrl(string action)
        {
            var urlHelper = new UrlHelper(ControllerContext.RequestContext);
            return urlHelper.Action(action, "Account", null, "https");
        }

        private string GetPasswordResetUrl(string email)
        {
            var baseUrl = GetBaseUrl("ResetPassword");

            return PasswordUtilities.GenerateResetPasswordUrl(baseUrl, email);
        }

        private static bool ValidateTokenParameters(UrlTokenParameters urlTokenParameters, out string errorMessage)
        {
            try
            {
                PasswordUtilities.ValidateResetPasswordParameters(urlTokenParameters);
            }
            catch (MissingParametersException)
            {
                errorMessage = Global.ResetPasswordMissingParametersException;
                return false;
            }
            catch (ExpiredTimestampException)
            {
                errorMessage = Global.ResetPasswordExpiredTimestampException;
                return false;
            }
            catch (InvalidTokenException)
            {
                errorMessage = Global.ResetPasswordInvalidTokenException;
                return false;
            }
            errorMessage = null;
            return true;
        }
    }
}
