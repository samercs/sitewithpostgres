﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Services;

namespace SiteWithPostgres.Web.Controllers
{
    [LanguageRoutePrefix("", "angular")]
    public class AngularController : ApplicationController
    {
        public AngularController(IAppServices appServices) : base(appServices)
        {
        }
        [Route("")]
        public ActionResult Index()
        {
            return View();
        }

        
    }
}