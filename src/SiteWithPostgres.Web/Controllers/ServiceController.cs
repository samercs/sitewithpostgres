using System.Linq;
using OrangeJetpack.Localization;
using System.Threading.Tasks;
using System.Web.Mvc;
using SiteWithPostgres.Services;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Services;

namespace SiteWithPostgres.Web.Controllers
{
    [LanguageRoutePrefix("", "our-service")]
    public class ServiceController : ApplicationController
    {
        private readonly ServiceManagmentService _managmentService;
        public ServiceController(IAppServices appServices) : base(appServices)
        {
            _managmentService = new ServiceManagmentService(DataContextFactory);
        }

        [Route("")]
        public async Task<ActionResult> Index()
        {
            var services = await _managmentService.GetAll();
            return View(services.Localize(LanguageCode, i=>i.Title, i=>i.Prev).ToList());
        }

        [Route("{id:int}")]
        public async Task<ActionResult> Detail(int id)
        {
            var service = await _managmentService.GetById(id);
            if (service == null)
            {
                return HttpNotFound();
            }

            return View(service.Localize(LanguageCode, i => i.Title, i => i.Description));
        }


    }
}
