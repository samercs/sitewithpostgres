using System.ComponentModel.DataAnnotations;

namespace SiteWithPostgres.Web.Areas.User.Models.Account
{
    public class EditAccountViewModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Required]
        public string Email { get; set; }

        [Required, StringLength(4, MinimumLength = 1)]
        public string PhoneCountryCode { get; set; }

        [Required, StringLength(15, MinimumLength = 7)]
        public string PhoneLocalNumber { get; set; }

        public PhoneNumberViewModel PhoneNumber { get; set; }
    }
}
