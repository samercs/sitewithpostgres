using SiteWithPostgres.Core.Regionalization.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace SiteWithPostgres.Web.Areas.User.Models
{
    public class PhoneNumberViewModel
    {
        private string _countryCode;

        [Required, StringLength(4, MinimumLength = 1)]
        public string PhoneCountryCode { get; set; }

        [Required, StringLength(15, MinimumLength = 7)]
        public string PhoneLocalNumber { get; set; }

        public string CountryCode
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(_countryCode))
                {
                    return _countryCode;
                }

                var country =
                    SiteWithPostgres.Core.Regionalization.Countries.GetAllCountries()
                        .FirstOrDefault(i => i.PhoneCountryCode == PhoneCountryCode);

                return country == null ? string.Empty : country.IsoCode;
            }
            set
            {
                _countryCode = value;
                var country =
                   SiteWithPostgres.Core.Regionalization.Countries.GetAllCountries()
                       .FirstOrDefault(i => string.Equals(i.IsoCode, _countryCode, StringComparison.CurrentCultureIgnoreCase));
                PhoneCountryCode = country == null ? string.Empty : country.PhoneCountryCode;
            }
        }

        public IReadOnlyCollection<Country> Countries => SiteWithPostgres.Core.Regionalization.Countries.GetAllCountries();
    }
}
