using SiteWithPostgres.Core.Email;
using SiteWithPostgres.Core.Enums;
using SiteWithPostgres.Core.Formate;
using SiteWithPostgres.Core.Service;
using SiteWithPostgres.Core.UI;
using SiteWithPostgres.Services;
using SiteWithPostgres.Services.Identity;
using SiteWithPostgres.Web.Areas.User.Models;
using SiteWithPostgres.Web.Areas.User.Models.Account;
using SiteWithPostgres.Web.Controllers;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Configuration;
using SiteWithPostgres.Web.Core.Services;
using SiteWithPostgres.Web.Extensions;
using SiteWithPostgres.Web.Resources;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SiteWithPostgres.Web.Areas.User.Controllers
{
    [RouteArea("user", AreaPrefix = "")]
    [LanguageRoutePrefix("user", "account")]
    public class AccountController : ApplicationController
    {
        private readonly IAuthService _authService;
        private readonly ICookieService _cookieService;
        private readonly UserService _userService;
        private readonly EmailTemplateService _emailTemplateService;
        private readonly IMessageService _messageService;
        private readonly IAppSettings _appSettings;

        public AccountController(IAppServices appServices) : base(appServices)
        {
            _authService = appServices.AuthService;
            _cookieService = appServices.CookieService;
            _userService = new UserService(DataContextFactory);
            _emailTemplateService = new EmailTemplateService(DataContextFactory);
            _messageService = appServices.MessageService;
            _appSettings = AppSettings;
        }

        [Route("")]
        public ActionResult MyAccount()
        {
            return View();
        }

        [Route("edit-account")]
        public async Task<ActionResult> EditAccount()
        {
            var user = await _authService.CurrentUser();
            var viewModel = new EditAccountViewModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = new PhoneNumberViewModel
                {
                    PhoneCountryCode = user.PhoneCountryCode,
                    PhoneLocalNumber = user.PhoneLocalNumber
                }

            };

            return View(viewModel);
        }

        [Route("edit-account")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAccount(EditAccountViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.PhoneNumber = new PhoneNumberViewModel
                {
                    PhoneCountryCode = model.PhoneCountryCode,
                    PhoneLocalNumber = model.PhoneLocalNumber
                };
                return View(model);
            }

            var user = await _authService.CurrentUser();

            var userNameChanged = user.UserName != model.Email;
            var originalEmail = user.Email;

            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Email = model.Email;
            user.UserName = model.Email;
            user.PhoneCountryCode = model.PhoneCountryCode;
            user.PhoneLocalNumber = model.PhoneLocalNumber;

            await _userService.UpdateUser(user);

            _cookieService.Add(CookieKeys.DisplayName, user.FirstName, DateTime.MaxValue);
            _cookieService.Add(CookieKeys.LastSignInEmail, user.Email, DateTime.MaxValue);

            if (userNameChanged)
            {
                // send to the new email
                user.Email += ";" + originalEmail;
                await SendEmailChangedEmail(user);

                _authService.LogOut();

                SetStatusMessage(Global.EditAccountNameAndEmailSuccessMessage);

                return RedirectToAction("Login", "Account");
            }

            SetStatusMessage(Global.EditAccountNameSuccessMessage);

            return RedirectToAction("MyAccount");
        }

        [Route("change-password")]
        public ActionResult ChangePassword()
        {
            var viewModel = new ChangePasswordViewModel();

            return View(viewModel);
        }

        [Route("change-password")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await _authService.ChangePassword(User.Identity, model.CurrentPassword, model.NewPassword);
            if (!result.Succeeded)
            {
                SetStatusMessage(Global.PasswordChangeFailureMessage, StatusMessageType.Error);

                return View(model);
            }

            await SendPasswordChangedEmail();

            SetStatusMessage(Global.PasswordSuccessfullyChanged);

            return RedirectToAction("MyAccount");
        }

        private async Task SendEmailChangedEmail(Entities.User user)
        {
            var emailTeamplet = await _emailTemplateService.GetByTemplateType(EmailTemplateType.EmailChanged, LanguageCode);
            var email = new Email
            {
                ToAddress = user.Email,
                Subject = emailTeamplet.Subject,
                Message = StringFormatter.ObjectFormat(emailTeamplet.Message, new { user.FirstName, _appSettings.SiteTitle })
            };

            await _messageService.Send(email.WithTemplate());
        }

        private async Task SendPasswordChangedEmail()
        {
            var user = await _authService.CurrentUser();
            var emailTeamplet = await _emailTemplateService.GetByTemplateType(EmailTemplateType.PasswordChanged, LanguageCode);
            var email = new Email
            {
                ToAddress = user.Email,
                Subject = emailTeamplet.Subject,
                Message = StringFormatter.ObjectFormat(emailTeamplet.Message, new { user.FirstName, _appSettings.SiteTitle })
            };

            await _messageService.Send(email.WithTemplate());
        }
    }
}
