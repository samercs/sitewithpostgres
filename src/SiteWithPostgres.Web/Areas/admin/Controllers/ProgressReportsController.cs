using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using SiteWithPostgres.Web.Areas.Admin.Controllers;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Services;

namespace SiteWithPostgres.Web.Areas.admin.Controllers
{
    [LanguageRoutePrefix("admin", "progress-reports")]
    public class ProgressReportsController : AdminBaseController
    {
        public ProgressReportsController(IAppServices appServices) : base(appServices)
        {
        }

        private static IReadOnlyCollection<FileInfo> GetMarkdownFiles()
        {
            var home = Environment.GetEnvironmentVariable("HOME");
            if (home == null)
            {
                throw new Exception("Cannot access HOME environmental variable.");
            }

            var docs = Path.Combine(home, @"site\wwwdocs");
            var dir = new DirectoryInfo(docs);

            return dir.GetFiles("*.md").OrderByDescending(i => i.Name).ToList();
        }

        [Route]
        public ActionResult Index()
        {
            return View(GetMarkdownFiles());
        }

        [Route("item")]
        public ActionResult Item(string file)
        {
            var fileInfo = GetMarkdownFiles().SingleOrDefault(i => i.Name == file);
            if (fileInfo == null)
            {
                return HttpNotFound();
            }

            return View(fileInfo);
        }
    }
}
