using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ImageResizer;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using OrangeJetpack.Localization;
using SiteWithPostgres.Entities;
using SiteWithPostgres.Services;
using SiteWithPostgres.Web.Areas.admin.Models.Services;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Services;

namespace SiteWithPostgres.Web.Areas.Admin.Controllers
{
    [LanguageRoutePrefix("admin", "services")]
    public class ServicesController : AdminBaseController
    {
        private readonly ServiceManagmentService _managmentService;
        public ServicesController(IAppServices appServices) : base(appServices)
        {
            _managmentService = new ServiceManagmentService(DataContextFactory);
        }

        [Route("")]
        public ActionResult Index()
        {
            var model = new IndexViewModel();
            return View(model);
        }

        [Route("get-data")]
        public async Task<JsonResult> GetData([DataSourceRequest] DataSourceRequest request, string search)
        {
            var data = await _managmentService.GetAll();
            
            return Json(data.Localize("en", i=>i.Title).ToDataSourceResult(request));
        }

        [Route("add")]
        public ActionResult Add()
        {
            var model = new AddEditViewModel()
            {
                Service = new Service()
                {
                    Title = LocalizedContent.Init(),
                    Description = LocalizedContent.Init(),
                    Prev = LocalizedContent.Init()
                }
            };
            return View(model);
        }

        [Route("add")]
        [HttpPost, ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Add(AddEditViewModel model, LocalizedContent[] title, LocalizedContent[] description, LocalizedContent[] prev)
        {
            //var resources = new Resource();
            //TryUpdateModel(resources);
            //if (!ModelState.IsValid)
            //{
            //    return View(model);
            //}

            model.Service.Title = title.Serialize();
            model.Service.Description = description.Serialize();
            model.Service.Prev = prev.Serialize();
            await _managmentService.Add(model.Service);
            SetStatusMessage("Service has been added successfully.");
            return RedirectToAction("Index");

        }

        [Route("{id:int}/edit")]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(int id)
        {

            var service = await _managmentService.GetById(id);
            if (service == null)
            {
                return HttpNotFound();
            }

            if (string.IsNullOrEmpty(service.Prev))
            {
                service.Prev = LocalizedContent.Init();
            }

            var model = new AddEditViewModel()
            {
                Service = service
            };
            return View(model);
        }

        [Route("{id:int}/edit")]
        [HttpPost, ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(int id, AddEditViewModel model, LocalizedContent[] title, LocalizedContent[] description, LocalizedContent[] prev)
        {
            model.Service.Title = title.Serialize();
            model.Service.Description = description.Serialize();
            model.Service.Prev = prev.Serialize();
            await _managmentService.Edit(model.Service);
            SetStatusMessage("Service has been saved successfully.");
            return RedirectToAction("Index");
        }

        [Route("{id:int}/delete")]
        public async Task<ActionResult> Delete(int id)
        {

            var image = await _managmentService.GetById(id);
            if (image == null)
            {
                return HttpNotFound();
            }

            return Confirmation("Delete Service", "Are you sure want to delete this service?");
        }

        [Route("{id:int}/delete")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, FormCollection model)
        {
            var image = await _managmentService.GetById(id);
            if (image == null)
            {
                return HttpNotFound();
            }

            await _managmentService.Remove(image);
            SetStatusMessage("Service has been removed successfully.");
            return RedirectToAction("Index");

        }

        [Route("upload-image")]
        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult UploadImage(HttpPostedFileBase file)
        {
            try
            {
                var filename = Guid.NewGuid() + Path.GetExtension(file.FileName);
                file.SaveAs(Server.MapPath("~/img/service/" + filename));
                ImageBuilder.Current.Build(Server.MapPath("~/img/service/" + filename),
                    Server.MapPath("~/img/service/" + filename), new ResizeSettings()
                    {
                        Width = 500,
                        Height = 500,
                        Format = Path.GetExtension(file.FileName).Replace(".", ""),
                        Mode = FitMode.None,
                        Quality = 80,
                        BackgroundColor = Color.Black

                    });
                return Json(new
                {
                    itemId = filename,
                    url = "/img/service/" + filename,
                    baseUrl = filename
                });
            }
            catch (Exception)
            {
                return JsonError("Could not upload image.");
            }
        }

        [Route("delete-image")]
        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult DeleteImage(string itemId)
        {
            if (System.IO.File.Exists(Server.MapPath("~/img/service/" + itemId)))
            {
                System.IO.File.Delete(Server.MapPath("~/img/service/" + itemId));
            }
            return Json(HttpStatusCode.OK);
        }

    }
}
