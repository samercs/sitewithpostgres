using System;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using OrangeJetpack.Localization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using SiteWithPostgres.Core.UI;
using SiteWithPostgres.Entities;
using SiteWithPostgres.Services;
using SiteWithPostgres.Web.Areas.Admin.Models.Pages;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Services;

namespace SiteWithPostgres.Web.Areas.Admin.Controllers
{
    [LanguageRoutePrefix("admin", "pages")]
    public class PagesController : AdminBaseController
    {
        private readonly PageService _pageService;

        public PagesController(IAppServices appServices) : base(appServices)
        {
            _pageService = new PageService(DataContextFactory);
        }

        [Route("")]
        public ActionResult Index()
        {
            var model = new IndexViewModel();
            return View(model);
        }

        [Route("get-data")]
        public async Task<JsonResult> GetData([DataSourceRequest] DataSourceRequest request, string search)
        {
            var data = await _pageService.GetAll();
            data = data.Localize(LanguageCode, i => i.Title).ToList();
            return Json(data.ToDataSourceResult(request));
        }

        [Route("add")]
        public ActionResult Add()
        {
            var model = new AddEditViewModel()
            {
                Page = new Page()
                {
                    Title = LocalizedContent.Init(),
                    Content = LocalizedContent.Init(),
                    Preview = LocalizedContent.Init()
                }
            };
            return View(model);
        }

        [Route("add")]
        [HttpPost, ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Add(AddEditViewModel model, LocalizedContent[] title)
        {
            if (ModelState.ContainsKey("Page.Title"))
                ModelState["Page.Title"].Errors.Clear();
            if (!ModelState.IsValid)
            {
                model.Page.Title = LocalizedContent.Init();
                return View(model);
            }

            model.Page.Title = title.Serialize();
            model.Page.Content = LocalizedContent.Init();
            model.Page.Preview = LocalizedContent.Init();
            await _pageService.Add(model.Page);
            SetStatusMessage("Page has been added successfully.");
            return RedirectToAction("Index");

        }


        [Route("{id:int}/edit")]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(int id)
        {

            var page = await _pageService.GetById(id);
            if (page == null)
            {
                return HttpNotFound();
            }

            var model = new AddEditViewModel()
            {
                Page = page
            };
            return View(model);
        }

        [Route("{id:int}/edit")]
        [HttpPost, ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(int id, AddEditViewModel model, LocalizedContent[] title, LocalizedContent[] content, LocalizedContent[] preview)
        {
            try
            {
                model.Page.Title = title.Serialize();
                model.Page.Content = content.Serialize();
                model.Page.Preview = preview.Serialize();
                await _pageService.Edit(model.Page);
                SetStatusMessage("Page has been saved successfully.");
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                SetStatusMessage("Sorry, Unable to update page please try agine.", StatusMessageType.Error);
                return RedirectToAction("Edit", new {id = id});
            }
            

        }

        [Route("{id:int}/delete")]
        public async Task<ActionResult> Delete(int id)
        {

            var page = await _pageService.GetById(id);
            if (page == null)
            {
                return HttpNotFound();
            }

            return Confirmation("Delete Page", "Are you sure want to delete this page?");
        }

        [Route("{id:int}/delete")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, FormCollection model)
        {
            var page = await _pageService.GetById(id);
            if (page == null)
            {
                return HttpNotFound();
            }

            await _pageService.Remove(page);
            SetStatusMessage("Page has been removed successfully.");
            return RedirectToAction("Index");

        }


    }
}
