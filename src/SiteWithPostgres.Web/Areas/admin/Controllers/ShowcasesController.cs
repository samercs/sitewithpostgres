using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SiteWithPostgres.Entities;
using SiteWithPostgres.Entities.Enum;
using SiteWithPostgres.Services;
using SiteWithPostgres.Web.Areas.admin.Models.Showcases;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Services;
using OrangeJetpack.Localization;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SiteWithPostgres.Web.Areas.Admin.Controllers
{
    [LanguageRoutePrefix("admin", "showcases")]
    public class ShowcasesController : AdminBaseController
    {
        private readonly SlideService _slideService;
        public ShowcasesController(IAppServices appServices) : base(appServices)
        {
            _slideService = new SlideService(DataContextFactory);
        }
        [Route("")]
        public ActionResult Index()
        {
            var model = new IndexViewModel();
            return View(model);
        }

        [Route("get-data")]
        public async Task<JsonResult> GetData([DataSourceRequest] DataSourceRequest request, string search)
        {
            var data = await _slideService.GetAll();
            data = data.Localize(LanguageCode, i => i.Title).ToList();
            return Json(data.ToDataSourceResult(request));
        }

        [Route("add")]
        public ActionResult Add()
        {
            var model = new AddEditViewModel()
            {
                Showcase = new Slide()
                {
                    Title = LocalizedContent.Init()
                }
            };
            return View(model);
        }

        [Route("add")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddEditViewModel model, LocalizedContent[] title)
        {
            if (ModelState.ContainsKey("Showcase.Title"))
                ModelState["Showcase.Title"].Errors.Clear();

            if (!ModelState.IsValid)
            {
                model.Showcase.Title = LocalizedContent.Init();
                return View(model);
            }

            model.Showcase.Title = title.Serialize();
            model.Showcase.Title2 = LocalizedContent.Init();
            model.Showcase.Title3 = LocalizedContent.Init();
            model.Showcase.ItemStatus = ItemStatus.Public;
            var addedEntity = await _slideService.Add(model.Showcase);
            SetStatusMessage("Showcase has been added successfully.");
            return RedirectToAction("Edit", new {id= addedEntity.SlideId});

        }

        [Route("upload-image")]
        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult UploadImage(HttpPostedFileBase file)
        {
            try
            {
                var filename = Guid.NewGuid() + Path.GetExtension(file.FileName);
                file.SaveAs(Server.MapPath("~/SystemDir/Showcases/" + filename));

                return Json(new
                {
                    itemId = filename,
                    url = "/SystemDir/Showcases/" + filename,
                    baseUrl = filename
                });
            }
            catch (Exception)
            {
                return JsonError("Could not upload image.");
            }
        }

        [Route("delete-image")]
        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult DeleteImage(string itemId)
        {
            if (System.IO.File.Exists(Server.MapPath("~/SystemDir/Showcases/" + itemId)))
            {
                System.IO.File.Delete(Server.MapPath("~/SystemDir/Showcases/" + itemId));
            }
            return Json(HttpStatusCode.OK);
        }

        [Route("{id:int}/edit")]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(int id)
        {

            var slide = await _slideService.GetById(id);
            if (slide == null)
            {
                return HttpNotFound();
            }

            var model = new AddEditViewModel()
            {
                Showcase = slide
            };
            return View(model);
        }

        [Route("{id:int}/edit")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, AddEditViewModel model, LocalizedContent[] title, LocalizedContent[] title2, LocalizedContent[] title3)
        {
            model.Showcase.Title = title.Serialize();
            model.Showcase.Title2 = title2.Serialize();
            model.Showcase.Title3 = title3.Serialize();
            await _slideService.Edit(model.Showcase);
            SetStatusMessage("Showcase has been saved successfully.");
            return RedirectToAction("Index");

        }

        [Route("{id:int}/delete")]
        public async Task<ActionResult> Delete(int id)
        {

            var slide = await _slideService.GetById(id);
            if (slide == null)
            {
                return HttpNotFound();
            }

            return Confirmation("Delete Showcase", "Are you sure want to delete this showcase?");
        }

        [Route("{id:int}/delete")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, FormCollection model)
        {
            var slide = await _slideService.GetById(id);
            if (slide == null)
            {
                return HttpNotFound();
            }

            await _slideService.Remove(slide);
            SetStatusMessage("Showcase has been removed successfully.");
            return RedirectToAction("Index");

        }


    }
}
