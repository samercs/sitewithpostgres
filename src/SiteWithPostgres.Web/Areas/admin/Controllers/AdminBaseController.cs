using System.Web.Mvc;
using SiteWithPostgres.Web.Controllers;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Identity;
using SiteWithPostgres.Web.Core.Services;

namespace SiteWithPostgres.Web.Areas.Admin.Controllers
{
    [RoleAuthorize(Role.Administrator)]
    [RouteArea("admin", AreaPrefix = "")]
    public class AdminBaseController : ApplicationController
    {
        public AdminBaseController(IAppServices appServices) : base(appServices)
        {
        }
    }
}
