using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SiteWithPostgres.Services;
using SiteWithPostgres.Web.Areas.admin.Models.Contacts;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Services;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SiteWithPostgres.Web.Areas.Admin.Controllers
{
    [LanguageRoutePrefix("admin", "contacts")]
    public class ContactsController : AdminBaseController
    {
        private readonly ContactService _contactService;
        public ContactsController(IAppServices appServices) : base(appServices)
        {
            _contactService = new ContactService(DataContextFactory);
        }

        [Route("")]
        public ActionResult Index()
        {
            var model = new IndexViewModel();
            return View(model);
        }

        [Route("get-data")]
        public async Task<JsonResult> GetData([DataSourceRequest] DataSourceRequest request, string search)
        {
            var data = await _contactService.GetAll();
            return Json(data.ToDataSourceResult(request));
        }
        [Route("{id:int}/read")]
        public async Task<ActionResult> Read(int id)
        {
            var contact = await _contactService.GetById(id);
            contact.IsRead = true;
            await _contactService.Edit(contact);
            return View(contact);
        }
        [Route("{id:int}/delete")]
        public async Task<ActionResult> Delete(int id)
        {

            var contact = await _contactService.GetById(id);
            if (contact == null)
            {
                return HttpNotFound();
            }

            return Confirmation("Delete Message", "Are you sure want to delete this message?");
        }

        [Route("{id:int}/delete")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, FormCollection model)
        {
            var contact = await _contactService.GetById(id);
            if (contact == null)
            {
                return HttpNotFound();
            }

            await _contactService.Remove(contact);
            SetStatusMessage("Message has been removed successfully.");
            return RedirectToAction("Index");

        }

    }
}
