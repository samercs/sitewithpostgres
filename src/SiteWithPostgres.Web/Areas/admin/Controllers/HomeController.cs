﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Services;

namespace SiteWithPostgres.Web.Areas.Admin.Controllers
{
    [LanguageRoutePrefix("admin", "home")]
    public class HomeController : AdminBaseController
    {
        public HomeController(IAppServices appServices) : base(appServices)
        {
        }

        [Route("")]
        public ActionResult Index()
        {
            return View();
        }

        [Route("state-color")]
        public ActionResult StateColor()
        {
            return View();
        }
    }
}