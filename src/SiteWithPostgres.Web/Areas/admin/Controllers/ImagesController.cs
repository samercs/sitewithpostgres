using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SiteWithPostgres.Entities;
using SiteWithPostgres.Entities.Enum;
using SiteWithPostgres.Services;
using SiteWithPostgres.Web.Areas.admin.Models.Image;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Services;
using OrangeJetpack.Localization;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SiteWithPostgres.Web.Areas.Admin.Controllers
{
    [LanguageRoutePrefix("admin", "images")]
    public class ImagesController : AdminBaseController
    {
        private readonly ImageService _imageService;
        public ImagesController(IAppServices appServices) : base(appServices)
        {
            _imageService = new ImageService(DataContextFactory);
        }

        [Route("")]
        public ActionResult Index()
        {
            var model = new IndexViewModel();
            return View(model);
        }

        [Route("get-data")]
        public async Task<JsonResult> GetData([DataSourceRequest] DataSourceRequest request, string search)
        {
            var data = await _imageService.GetAll();
            data = data.Localize(LanguageCode, i => i.Title).ToList();
            return Json(data.ToDataSourceResult(request));
        }
        [Route("add")]
        public ActionResult Add()
        {
            var model = new AddEditViewModel()
            {
                Image = new Image()
                {
                    Title = LocalizedContent.Init(),
                    ImageKey = ""

                }
            };
            return View(model);
        }

        [Route("add")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddEditViewModel model, LocalizedContent[] title)
        {
            if (ModelState.ContainsKey("Image.Title"))
                ModelState["Image.Title"].Errors.Clear();
            if (ModelState.ContainsKey("Image.Status"))
                ModelState["Image.Status"].Errors.Clear();

            if (!ModelState.IsValid)
            {
                model.Image.Title = LocalizedContent.Init();
                return View(model);
            }

            model.Image.Title = title.Serialize();
            model.Image.Status = ItemStatus.Public;
            await _imageService.Add(model.Image);
            SetStatusMessage("Image has been added successfully.");
            return RedirectToAction("Index");

        }

        [Route("{id:int}/edit")]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(int id)
        {

            var image = await _imageService.GetById(id);
            if (image == null)
            {
                return HttpNotFound();
            }

            var model = new AddEditViewModel()
            {
                Image = image
            };
            return View(model);
        }

        [Route("{id:int}/edit")]
        [HttpPost, ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(int id, AddEditViewModel model, LocalizedContent[] title)
        {
            model.Image.Title = title.Serialize();
            await _imageService.Edit(model.Image);
            SetStatusMessage("Image has been saved successfully.");
            return RedirectToAction("Index");

        }


        [Route("upload-image")]
        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult UploadImage(HttpPostedFileBase file)
        {
            try
            {
                var filename = Guid.NewGuid() + Path.GetExtension(file.FileName);
                file.SaveAs(Server.MapPath("~/SystemDir/Images/" + filename));

                return Json(new
                {
                    itemId = filename,
                    url = "/SystemDir/Images/" + filename,
                    baseUrl = filename
                });
            }
            catch (Exception)
            {
                return JsonError("Could not upload image.");
            }
        }

        [Route("delete-image")]
        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult DeleteImage(string itemId)
        {
            if (System.IO.File.Exists(Server.MapPath("~/SystemDir/Images/" + itemId)))
            {
                System.IO.File.Delete(Server.MapPath("~/SystemDir/Images/" + itemId));
            }
            return Json(HttpStatusCode.OK);
        }

        [Route("{id:int}/delete")]
        public async Task<ActionResult> Delete(int id)
        {

            var image = await _imageService.GetById(id);
            if (image == null)
            {
                return HttpNotFound();
            }

            return Confirmation("Delete Image", "Are you sure want to delete this image?");
        }

        [Route("{id:int}/delete")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, FormCollection model)
        {
            var image = await _imageService.GetById(id);
            if (image == null)
            {
                return HttpNotFound();
            }

            await _imageService.Remove(image);
            SetStatusMessage("Image has been removed successfully.");
            return RedirectToAction("Index");

        }

    }
}
