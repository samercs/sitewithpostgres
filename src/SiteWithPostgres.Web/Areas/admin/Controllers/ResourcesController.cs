using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SiteWithPostgres.Entities;
using SiteWithPostgres.Services;
using SiteWithPostgres.Web.Areas.admin.Models.Resource;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Services;
using OrangeJetpack.Localization;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SiteWithPostgres.Web.Areas.Admin.Controllers
{
    [LanguageRoutePrefix("admin", "resources")]
    public class ResourcesController : AdminBaseController
    {
        private readonly ResourceService _resourceService;
        private readonly ILanguageService _languageService;
        public ResourcesController(IAppServices appServices) : base(appServices)
        {
            _resourceService = new ResourceService(DataContextFactory);
            _languageService = appServices.LanguageService;
        }

        [Route("")]
        public ActionResult Index()
        {
            var model = new IndexViewModel();
            return View(model);
        }

        [Route("get-data")]
        public async Task<JsonResult> GetData([DataSourceRequest] DataSourceRequest request, string search)
        {
            var data = await _resourceService.GetAll();
            return Json(data.ToDataSourceResult(request));
        }

        [Route("add")]
        public ActionResult Add()
        {
            var model = new AddEditViewModel()
            {
                Resource = new Resource()
                {
                    Value = LocalizedContent.Init()
                }
            };
            return View(model);
        }

        [Route("add")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddEditViewModel model, LocalizedContent[] value)
        {
            //var resources = new Resource();
            //TryUpdateModel(resources);
            //if (!ModelState.IsValid)
            //{
            //    return View(model);
            //}

            model.Resource.Value = value.Serialize();
            await _resourceService.Add(model.Resource);
            SetStatusMessage("Resources has been added successfully.");
            return RedirectToAction("Index");

        }


        [Route("{id:int}/edit")]
        public async Task<ActionResult> Edit(int id)
        {

            var resources = await _resourceService.GetById(id);
            if (resources == null)
            {
                return HttpNotFound();
            }

            var model = new AddEditViewModel()
            {
                Resource = resources
            };
            return View(model);
        }

        [Route("{id:int}/edit")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, AddEditViewModel model, LocalizedContent[] value)
        {
            model.Resource.Value = value.Serialize();
            await _resourceService.Edit(model.Resource);
            SetStatusMessage("Resources has been saved successfully.");
            return RedirectToAction("Index");

        }

        [Route("{id:int}/delete")]
        public async Task<ActionResult> Delete(int id)
        {

            var resources = await _resourceService.GetById(id);
            if (resources == null)
            {
                return HttpNotFound();
            }

            return Confirmation("Delete Resource", "Are you sure want to delete this resource?");
        }

        [Route("{id:int}/delete")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, FormCollection model)
        {
            var resources = await _resourceService.GetById(id);
            if (resources == null)
            {
                return HttpNotFound();
            }

            await _resourceService.Remove(resources);
            SetStatusMessage("Resources has been removed successfully.");
            return RedirectToAction("Index");

        }

        [Route("reload-cach")]
        public ActionResult ReloadCach()
        {
            LanguageService.LoadResourceFromServer();
            SetStatusMessage("The latest version of resources file has been upload to cach.");
            return RedirectToAction("Index");

        }


    }
}
