using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using OrangeJetpack.Localization;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using SiteWithPostgres.Core.UI;
using SiteWithPostgres.Entities;
using SiteWithPostgres.Services;
using SiteWithPostgres.Web.Areas.admin.Models.Links;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Services;

namespace SiteWithPostgres.Web.Areas.Admin.Controllers
{
    [LanguageRoutePrefix("admin", "links")]
    public class LinksController : AdminBaseController
    {
        private readonly LinkService _service;

        public LinksController(IAppServices appServices) : base(appServices)
        {
            _service = new LinkService(DataContextFactory);
        }

        [Route("")]
        public ActionResult Index()
        {
            var model = new IndexViewModel();
            return View(model);
        }

        [Route("get-data")]
        public async Task<JsonResult> GetData([DataSourceRequest] DataSourceRequest request, string search)
        {
            var data = await _service.GetAll();
            data = data.Localize(LanguageCode, i => i.Title).ToList();
            return Json(data.ToDataSourceResult(request));
        }

        [Route("add")]
        public ActionResult Add()
        {
            var model = new AddEditViewModel()
            {
                Link = new Link()
                {
                    Title = LocalizedContent.Init()
                }
            };
            return View(model);
        }

        [Route("add")]
        [HttpPost, ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Add(AddEditViewModel model, LocalizedContent[] title)
        {
            if (ModelState.ContainsKey("Link.Title"))
                ModelState["Link.Title"].Errors.Clear();
            if (!ModelState.IsValid)
            {
                model.Link.Title = LocalizedContent.Init();
                return View(model);
            }

            model.Link.Title = title.Serialize();
            await _service.Add(model.Link);
            SetStatusMessage("Link has been added successfully.");
            return RedirectToAction("Index");

        }


        [Route("{id:int}/edit")]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(int id)
        {

            var link = await _service.GetById(id);
            if (link == null)
            {
                return HttpNotFound();
            }

            var model = new AddEditViewModel()
            {
                Link = link
            };
            return View(model);
        }

        [Route("{id:int}/edit")]
        [HttpPost, ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(int id, AddEditViewModel model, LocalizedContent[] title)
        {
            try
            {
                model.Link.Title = title.Serialize();
                await _service.Edit(model.Link);
                SetStatusMessage("Link has been saved successfully.");
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                SetStatusMessage("Sorry, Unable to update page please try agine.", StatusMessageType.Error);
                return RedirectToAction("Edit", new { id = id });
            }


        }
        
        [Route("{id:int}/delete")]
        public async Task<ActionResult> Delete(int id)
        {

            var link = await _service.GetById(id);
            if (link == null)
            {
                return HttpNotFound();
            }

            return Confirmation("Delete Link", "Are you sure want to delete this link?");
        }

        [Route("{id:int}/delete")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, FormCollection model)
        {
            var link = await _service.GetById(id);
            if (link == null)
            {
                return HttpNotFound();
            }

            await _service.Remove(link);
            SetStatusMessage("Link has been removed successfully.");
            return RedirectToAction("Index");
       
        }


    }
}
