using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using SiteWithPostgres.Core.Formate;
using SiteWithPostgres.Services.Identity;
using SiteWithPostgres.Web.Areas.admin.Models.Users;
using SiteWithPostgres.Web.Areas.Admin.Controllers;
using SiteWithPostgres.Web.Areas.Admin.Models.Users;
using SiteWithPostgres.Web.Core.ActionResults;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Identity;
using SiteWithPostgres.Web.Core.Services;

namespace SiteWithPostgres.Web.Areas.admin.Controllers
{
    [LanguageRoutePrefix("admin", "users")]
    public class UsersController : AdminBaseController
    {
        private readonly UserService _userService;

        public UsersController(IAppServices appServices) : base(appServices)
        {
            _userService = new UserService(DataContextFactory);
        }

        [Route]
        public ActionResult Index(string search = "", Role? role = null)
        {
            var viewModel = new IndexViewModel
            {
                Search = search,
                Role = role,
                RoleList = GetRoleList()
            };

            return View(viewModel);
        }

        private static SelectList GetRoleList()
        {
            var roles = from Role r in Enum.GetValues(typeof (Role))
                select new
                {
                    Id = r, 
                    Name = EnumFormatter.Description(r)
                };
            
            return new SelectList(roles, "Id", "Name");
        }

        [Route("json")]
        public JsonResult GetUsers([DataSourceRequest] DataSourceRequest request, string search = null, Role? role = null)
        {
            for (var i = 0; i < request.Sorts.Count; i++)
            {
                if (request.Sorts[i].Member == "FullName")
                {
                    request.Sorts[i].Member = "LastName";
                    request.Sorts.Add(new SortDescriptor("FirstName", request.Sorts[i].SortDirection));
                }
            }

            var users = _userService.GetUsersByQuery(search, role.ToString(), i => i.ToDataSourceResult(request));
            users.Data = users.Data.Cast<Entities.User>().Select(UserViewModel.Create);

            return Json(users);
        }

        [Route("excel")]
        public async Task<ExcelResult> UsersExcel(string search = "", Role? role = null)
        {
            var usersList = await _userService.GetUsers(search, role.ToString());
            var users = usersList.Select(UserViewModel.Create).ToList();

            return ExcelResult(users, "users");
        }

        [Route("{userId}/details")]
        public async Task<ActionResult> Details(string userId)
        {
            var user = await _userService.GetUserById(userId);
            if (user == null)
            {
                return HttpNotFound();
            }

            var model = await new DetailsViewModel().Hydrate(user, _userService);

            return View(model);
        }

        [Route("{userId}/details")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Details(string userId, DetailsViewModel model, string[] selectedRoles)
        {
            var user = await _userService.GetUserById(userId);
            if (user == null)
            {
                return HttpNotFound();
            }

            if (!ModelState.IsValid)
            {
                await model.Hydrate(user, _userService);

                return View("Details", model);
            }

            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.UserName = model.Email;
            user.Email = model.Email;
            
            await UpdateRoles(userId, selectedRoles);
            await _userService.UpdateUser(user);

            SetStatusMessage($"User {user.UserName} successfully updated.");

            return RedirectToAction("Details", new { id = userId });
        }

        private async Task UpdateRoles(string userId, IEnumerable<string> selectedRoles)
        {
            await RemoveUserFromRoles(userId);

            if (selectedRoles != null)
            {
                await AddUserToRoles(userId, selectedRoles);
            }
        }

        private async Task RemoveUserFromRoles(string userId)
        {
            var roles = await _userService.GetAllRoles();
            foreach (var role in roles)
            {
                await _userService.RemoveUserFromRole(userId, role.Name);
            }
        }

        private async Task AddUserToRoles(string userId, IEnumerable<string> roles)
        {
            foreach (var role in roles)
            {
                await _userService.AddUserToRole(userId, role.Replace(" ", ""));
            }
        }
	}
}
