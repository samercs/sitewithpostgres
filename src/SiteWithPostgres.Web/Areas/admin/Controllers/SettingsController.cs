using SiteWithPostgres.Core.Enums;
using SiteWithPostgres.Core.Formate;
using SiteWithPostgres.Web.Areas.Admin.Controllers;
using SiteWithPostgres.Web.Core.Attributes;
using SiteWithPostgres.Web.Core.Services;
using OrangeJetpack.Localization;
using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SiteWithPostgres.Web.Areas.admin.Controllers
{
    [LanguageRoutePrefix("admin", "settings")]
    public class SettingsController : AdminBaseController
    {
        public SettingsController(IAppServices appServices) : base(appServices)
        {
        }

        [Route("environmental-variables")]
        public ActionResult EnvironmentalVariables()
        {
            var variables = Environment.GetEnvironmentVariables()
                .Cast<DictionaryEntry>()
                .ToDictionary(i => (string)i.Key, i => (string)i.Value);

            return View(variables);
        }

        [Route("email-templates")]
        public async Task<ActionResult> EmailTemplates()
        {
            var model = await EmailTemplateService.GetAll();
            return View(model);
        }

        [Route("edit-email-template")]
        public async Task<ActionResult> EditEmailTemplate(EmailTemplateType id)
        {
            var template = await EmailTemplateService.GetByTemplateType(id);
            if (template != null)
            {
                return View(template);
            }
            return RedirectToAction("EmailTemplates");

        }

        [Route("edit-email-template")]
        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public async Task<ActionResult> EditEmailTemplate(EmailTemplateType id, LocalizedContent[] subject, LocalizedContent[] message)
        {
            var template = await EmailTemplateService.GetByTemplateType(id);
            if (template != null)
            {
                template.Subject = subject.Serialize();
                foreach (var localizedContent in message)
                {
                    localizedContent.Value = StringFormatter.StripWordHtml(localizedContent.Value);
                }

                template.Message = message.Serialize();

                await EmailTemplateService.Save(template);
                SetStatusMessage("Email template has been edited successfully.");
                return RedirectToAction("EmailTemplates");

            }
            return RedirectToAction("EmailTemplates");
        }

        [Route("api-key")]
        public ActionResult ApiKey()
        {
            var apiKey = AppSettings.ApiKey;
            return View("ApiKey", model: apiKey);
        }
    }
}
