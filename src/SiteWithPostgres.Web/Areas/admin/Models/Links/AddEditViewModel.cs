using SiteWithPostgres.Entities;

namespace SiteWithPostgres.Web.Areas.admin.Models.Links
{
    public class AddEditViewModel
    {
        public Link Link { get; set; }
    }
}
