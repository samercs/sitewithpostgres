namespace SiteWithPostgres.Web.Areas.admin.Models.Resource
{
    public class AddEditViewModel
    {
        public Entities.Resource Resource { get; set; }
    }
}
