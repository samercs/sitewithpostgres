using SiteWithPostgres.Core.Formate;
using System;

namespace SiteWithPostgres.Web.Areas.Admin.Models.Users
{
    public class UserViewModel
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public DateTime CreatedUtc { get; set; }

        public static UserViewModel Create(Entities.User user)
        {
            return new UserViewModel
            {
                UserId = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                FullName = NameFormatter.GetLastNameCommaFirstName(user.FirstName, user.LastName),
                Email = user.Email,
                CreatedUtc = user.CreatedUtc.AddHours(3) // UTC -> AST
            };
        }
    }
}
