using System.Web.Mvc;
using SiteWithPostgres.Web.Core.Identity;

namespace SiteWithPostgres.Web.Areas.admin.Models.Users
{
    public class IndexViewModel
    {
        public string Search { get; set; }
        public SelectList RoleList { get; set; }
        public Role? Role { get; set; }
    }
}
