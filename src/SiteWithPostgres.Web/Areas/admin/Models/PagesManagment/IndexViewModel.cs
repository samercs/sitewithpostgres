namespace SiteWithPostgres.Web.Areas.Admin.Models.PagesManagment
{
    public class IndexViewModel
    {
        public string Search { get; set; }
    }
}
