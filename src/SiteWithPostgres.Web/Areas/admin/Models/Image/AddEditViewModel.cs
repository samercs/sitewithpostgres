namespace SiteWithPostgres.Web.Areas.admin.Models.Image
{
    public class AddEditViewModel
    {
        public Entities.Image Image { get; set; }
    }
}
