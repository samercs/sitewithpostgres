namespace SiteWithPostgres.Web.Areas.Admin.Models.Pages
{
    public class AddEditViewModel
    {
        public Entities.Page Page { get; set; }
    }
}
