using SiteWithPostgres.Entities;

namespace SiteWithPostgres.Web.Areas.admin.Models.Showcases
{
    public class AddEditViewModel
    {
        public Slide Showcase { get; set; }
    }
}
