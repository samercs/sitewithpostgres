namespace SiteWithPostgres.Web.Areas.admin.Models.Showcases
{
    public class IndexViewModel
    {
        public string Search { get; set; }
    }
}
