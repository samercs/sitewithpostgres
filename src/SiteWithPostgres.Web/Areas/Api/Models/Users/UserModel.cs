using System.ComponentModel.DataAnnotations;

namespace SiteWithPostgres.Web.Areas.Api.Models.Users
{
    public class UserModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required, StringLength(256)]
        public string Email { get; set; }
    }
}
