namespace SiteWithPostgres.Web.Areas.Api.ErrorHandling
{
    public enum ApiErrorType
    {
        ModelStateError,
        ArgumentsError,
        IdentityResultError
    }
}
