using SiteWithPostgres.Core.Email;
using SiteWithPostgres.Core.Enums;
using SiteWithPostgres.Core.Formate;
using SiteWithPostgres.Core.Security;
using SiteWithPostgres.Core.Service;
using SiteWithPostgres.Entities;
using SiteWithPostgres.Services;
using SiteWithPostgres.Services.Identity;
using SiteWithPostgres.Web.Areas.Api.Models.Users;
using SiteWithPostgres.Web.Areas.User.Models.Account;
using SiteWithPostgres.Web.Core.Services;
using SiteWithPostgres.Web.Extensions;
using SiteWithPostgres.Web.Models.Account;
using SiteWithPostgres.Web.Resources;
using OrangeJetpack.Localization;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using SiteWithPostgres.Web.Areas.Api.Filters;

namespace SiteWithPostgres.Web.Areas.Api.Controllers
{
    [LanguageFilter]
    [RoutePrefix("api/users")]
    public class UsersController : ApplicationApiController
    {
        private readonly IMessageService _messageService;
        private readonly IAuthService _authService;
        private readonly UserService _userService;
        private readonly EmailTemplateService _emailTemplateService;

        public UsersController(IAppServices appServices) : base(appServices)
        {
            _messageService = appServices.MessageService;
            _authService = appServices.AuthService;
            _userService = new UserService(appServices.DataContextFactory);
            _emailTemplateService = new EmailTemplateService(DataContextFactory);
        }

        [Authorize]
        [HttpGet, Route("{id}", Name = "GetUser")]
        public async Task<IHttpActionResult> GetUser(string id)
        {
            var user = await _userService.GetUserById(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(new
            {
                user.Id,
                user.Email,
                user.FirstName,
                user.LastName,
                user.PhoneCountryCode,
                user.PhoneLocalNumber,
                user.CreatedUtc
            });
        }

        [Route(""), HttpPost]
        public async Task<IHttpActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return ModelStateError(ModelState);
            }

            var user = new Entities.User
            {
                UserName = model.Email,
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName
            };

            var result = await AuthService.CreateUser(user, model.Password);
            if (!result.Succeeded)
            {
                return IdentityResultError(result);
            }

            await SendNewUserWelcomeEmail(user);

            return CreatedAtRoute("GetUser", new { id = user.Id }, new
            {
                user.Id,
                user.Email,
                user.FirstName,
                user.LastName,
                user.PhoneCountryCode,
                user.PhoneLocalNumber,
                user.CreatedUtc
            });
        }

        [Route("{id}/password-reset"), HttpPost]
        public async Task<IHttpActionResult> RequestPasswordReset(NeedPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return ModelStateError(ModelState);
            }

            await SendPasswordResetNotification(model.Email);
            return Ok("Password has been sent");
        }

        [Authorize, HttpPut, Route("{id}")]
        public async Task<IHttpActionResult> UpdateUser(string id, UserModel model)
        {
            var user = await _userService.GetUserById(id);
            if (user == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return ModelStateError(ModelState);
            }

            var originalEmail = user.Email;
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Email = model.Email;

            await _userService.UpdateUser(user);

            if (originalEmail != user.Email)
            {
                await SendEmailChangedEmail(user, originalEmail);
            }

            return Ok();
        }

        [Authorize]
        [Route("{id}/password"), HttpPut]
        public async Task<IHttpActionResult> ChangePassword(string id, ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return ModelStateError(ModelState);
            }

            var result = await _authService.ChangePassword(User.Identity, model.CurrentPassword, model.NewPassword);
            if (!result.Succeeded)
            {
                return IdentityResultError(result);
            }

            await SendPasswordChangedEmail();

            return Ok();
        }

        private async Task SendNewUserWelcomeEmail(Entities.User user)
        {
            var template = await _emailTemplateService.GetByTemplateType(EmailTemplateType.AccountRegistration, LanguageCode);

            var email = new Email
            {
                ToAddress = user.Email,
                Subject = StringFormatter.ObjectFormat(template.Subject, new { AppSettings.SiteTitle }),
                Message = StringFormatter.ObjectFormat(template.Message, new { AppSettings.SiteTitle, user.FirstName })
            };

            try
            {
                await _messageService.Send(email.WithTemplate());
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
            }
        }

        private async Task SendEmailChangedEmail(Entities.User user, string originalEmail)
        {
            var template = await _emailTemplateService.GetByTemplateType(EmailTemplateType.EmailChanged);
            var email = new Email
            {
                ToAddress = originalEmail,
                Subject = template.Localize(LanguageCode, i => i.Subject).Subject,
                Message = StringFormatter.ObjectFormat(template.Localize(LanguageCode, i => i.Message).Message, new { user.FirstName, AppSettings.SiteTitle })
            };

            try
            {
                await _messageService.Send(email.WithTemplate());
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
            }
        }

        private async Task SendPasswordResetNotification(string emailAddress)
        {
            EmailTemplate template;
            var email = new Email
            {
                ToAddress = emailAddress,
                Subject = Global.ResetPassword
            };

            var user = await _userService.GetUserByEmail(emailAddress);
            if (user == null)
            {
                template = await _emailTemplateService.GetByTemplateType(EmailTemplateType.NoAccount, LanguageCode);
                email.Subject = template.Subject;
                email.Message = StringFormatter.ObjectFormat(template.Message, new { emailAddress });
            }
            else
            {
                var resetUrl = GetPasswordResetUrl(user.Email);

                template = await _emailTemplateService.GetByTemplateType(EmailTemplateType.PasswordReset, LanguageCode);
                email.Subject = template.Subject;
                email.Message = StringFormatter.ObjectFormat(template.Message, new
                {
                    EmailAddress = user.FirstName,
                    ResetPasswordUrl = resetUrl
                });
            }

            try
            {
                await _messageService.Send(email.WithTemplate());
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
            }
        }

        private string GetPasswordResetUrl(string email)
        {
            var baseUrl = GetBaseUrl("resetpassword");

            return PasswordUtilities.GenerateResetPasswordUrl(baseUrl, email);
        }

        private string GetBaseUrl(string action)
        {
            var baseUri = new Uri(Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, ""));

            var resourceRelative = "~/en/account/" + action;
            var resourceFullPath = new Uri(baseUri, VirtualPathUtility.ToAbsolute(resourceRelative));

            return resourceFullPath.AbsoluteUri;
        }

        private async Task SendPasswordChangedEmail()
        {
            var user = await _authService.CurrentUser();
            var template = await _emailTemplateService.GetByTemplateType(EmailTemplateType.PasswordChanged, LanguageCode);
            var email = new Email
            {
                ToAddress = user.Email,
                Subject = template.Subject,
                Message = StringFormatter.ObjectFormat(template.Message, new { user.FirstName, AppSettings.SiteTitle })
            };

            try
            {
                await _messageService.Send(email.WithTemplate());
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
            }
        }

        [Route("get-all"), HttpGet]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetAll()
        {
            var users = await _userService.GetUsers();
            return Ok(users.Select(i => new
            {
                i.Id,
                Name = NameFormatter.Formate(i.FirstName, i.LastName),
                i.Email,
                i.PhoneNumber,
                PhotoPath = $"{AppSettings.Url}/SystemDir/Users/{i.Image}"  
            }).ToList());
        }
    }
}
