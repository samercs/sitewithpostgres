using Microsoft.AspNet.Identity;
using SiteWithPostgres.Core.Logger;
using SiteWithPostgres.Data;
using SiteWithPostgres.Web.Areas.Api.ErrorHandling;
using SiteWithPostgres.Web.Areas.Api.Filters;
using SiteWithPostgres.Web.Core.Configuration;
using SiteWithPostgres.Web.Core.Services;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace SiteWithPostgres.Web.Areas.Api.Controllers
{
    [LanguageFilter]
    public class ApplicationApiController : ApiController
    {
        protected readonly IDataContextFactory DataContextFactory;
        protected readonly IAuthService AuthService;
        protected readonly IErrorLogger ErrorLogger;
        protected readonly IAppSettings AppSettings;

        public string LanguageCode { get; set; }

        protected ApplicationApiController(IAppServices appServices)
        {
            DataContextFactory = appServices.DataContextFactory;
            AuthService = appServices.AuthService;
            ErrorLogger = appServices.ErrorLogger;
            AppSettings = appServices.AppSettings;
        }

        public IHttpActionResult ModelStateError(ModelStateDictionary modelState)
        {
            return new ErrorActionResult(new ApiError
            {
                Type = ApiErrorType.ModelStateError,
                Message = "Model state is invalid.",
                Metadata = new
                {
                    fields = modelState.Keys.ToArray()
                }
            });
        }
        public IHttpActionResult InvalidArgumentError(object parameter)
        {
            return new ErrorActionResult(new ApiError
            {
                Type = ApiErrorType.ArgumentsError,
                Message = "Invalid parameter.",
                Metadata = new
                {
                    parameter = parameter
                }
            });
        }

        public IHttpActionResult IdentityResultError(IdentityResult identityResult)
        {
            return new ErrorActionResult(new ApiError
            {
                Type = ApiErrorType.IdentityResultError,
                Message = "Identity result error.",
                Metadata = new
                {
                    errors = identityResult.Errors.ToArray()
                }
            });
        }

    }
}
