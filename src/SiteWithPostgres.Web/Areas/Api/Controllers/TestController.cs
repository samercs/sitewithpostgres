using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SiteWithPostgres.Web.Areas.Api.Controllers
{
    [RoutePrefix("api/test")]
    public class TestController : ApiController
    {
        [Route("")]
        [HttpGet]
        public IHttpActionResult Echo()
        {
            return Ok("Hello");
        }
    }
}
