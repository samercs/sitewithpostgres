using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace SiteWithPostgres.Web.Areas.Api.Authentication
{
    public class AuthenticationHandler: DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {

            IEnumerable<string> apiKeyValues;
            if (request.Headers.TryGetValues("X-ApiKey", out apiKeyValues))
            {
                var apiKeyValue = apiKeyValues.First();
                if (ConfigurationManager.AppSettings["ApiKey"] == apiKeyValue)
                {
                    return base.SendAsync(request, cancellationToken);
                }
            }

            return GetCustomResponse();
        }

        private static Task<HttpResponseMessage> GetCustomResponse()
        {
            var response = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("Api key is missing or invalid.")
            };

            var task = new TaskCompletionSource<HttpResponseMessage>();
            task.SetResult(response);
            return task.Task;
        }
    }
}
