﻿(($ => {

    var countryFlag = $("#AddressEntryFlag");

    var changeCountry = function () {

        const $this = $(this);
        var countryCode = $this.val();
        if (countryCode.length === 0) {
            return;
        }
        countryCode = countryCode.toLowerCase();
        countryFlag.removeClass().addClass("flag flag-" + countryCode);
    };

    var pickCountryCode = function () {
        $("#PhoneCountryCodeList").hide();
        $("#PhoneCountryCode").show();
        $("#PhoneCountryCode").val($(this).find('option:selected').data('phonecode'));
    }

    $(() => {
        $(".country-list").on("change", changeCountry);
        $("#PhoneCountryCodeList").hide();

        $("#PhoneCountryCode").mouseover(function () {
            $(this).hide();
            $("#PhoneCountryCodeList").show();
        });

        $("#PhoneCountryCodeList").mouseout(function () {
            if ($(this).is(":focus")) {
                return;
            }

            $(this).hide();
            $("#PhoneCountryCode").show();
            $("#PhoneCountryCode").val($(this).find('option:selected').data('phonecode'));

        });

        $("#PhoneCountryCodeList").bind("focusout change", pickCountryCode);
    });

})(jQuery));