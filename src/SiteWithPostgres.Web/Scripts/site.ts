﻿($ => {
    var testmonilasShowIndex = 0;
    var setHiddenLocalizedContent = function () {
        const $this = $(this);
        $this.siblings("input[type='hidden']").val($this.val());
    };

    var autoSubmitForm = function () {
        $(this).parents("form").submit();
    };

    var bindEvents = () => {
        $("form").on("change", ".auto-submit", autoSubmitForm);
        $(".localized-content").on("input", ".localized-input[data-primary='true']", setHiddenLocalizedContent);


        $(window).scroll(function (e) {


        });

        setTimeout(() => {
            $(".alert").parent().fadeOut(500);
        },
            10000);
        setInterval(() => {

        }, 10000);
    };



    var initRequiredLabels = () => {
        $(".required-label").slice(1).children("span").hide();
    }

    $(() => {
        bindEvents();
        initRequiredLabels();

    });


    $.fn.extend({
        animateCss: function (animationName, callback) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            this.addClass('animated ' + animationName).one(animationEnd, function () {
                $(this).removeClass('animated ' + animationName);
                if (callback) {
                    callback();
                }
            });
            return this;
        }
    });

})(jQuery);

module App {

    export class Kendo {
        static formatFileSize(size) {
            let e = Math.log(size) / Math.log(1024) | 0;
            return e.toFixed(2) + " " + (e ? "KMGTPEZY"[--e] + "B" : "bytes");
        }
    }

    export class CkEditor {

        static init() {
            // ReSharper disable SuspiciousThisUsage

            const editorHeight = 180;

            var fixValidation = function () {
                this.on("change", () => {
                    var instance = CKEDITOR.currentInstance;
                    var name = instance.name;
                    var data = instance.getData();
                    var parent = $(`li[data-for='${name}']`);
                    var inputs = parent.find(".localized-hidden, .localized-input");

                    if (data.length >= 0) {
                        inputs.val(data).addClass("valid");
                    } else {
                        inputs.val("").removeClass("valid");
                    }
                });
            }

            $("textarea.localized-input:not([data-language=ar])").each(function () {
                CKEDITOR.replace(this.id, {
                    height: editorHeight,
                    extraAllowedContent: "div(*){*}[*];*(page-*)",
                    stylesSet: "CustomStyles"
                }).on("instanceReady", fixValidation);
            });

            $("textarea.localized-input[data-language=ar]").each(function () {
                CKEDITOR.replace(this.id, {
                    height: editorHeight,
                    contentsLangDirection: "rtl",
                    extraAllowedContent: "div(*){*}[*];*(page-*)"
                });
            });

            $(".ckeditor").each(function () {
                CKEDITOR.replace(this.id, {
                    height: editorHeight,
                    contentsLangDirection: "rtl",
                    extraAllowedContent: "div(*){*}[*];*(page-*)"
                });
            });

            // ReSharper restore SuspiciousThisUsage
        }
    }
}