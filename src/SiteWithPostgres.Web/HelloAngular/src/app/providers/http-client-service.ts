// Angular references
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

// RxJS references
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
// tslint:disable-next-line:import-blacklist
import 'rxjs/Rx';

@Injectable()
export class HttpClientService {

    // Production
    private apiUrl = 'http://localhost:14353/api';
    private timeout = 300000;
    private apiKey = '6053RxaSo9KvDY3WtEXg';

    constructor(private http: Http) { }

    // Method that returns the current api url
    public getApiUrl(): string {
        return this.apiUrl;
    }

    // Method that returns the api key
    public getApiKey(): string {
        return this.apiKey;
    }

    // Method that creates the header options for get requests
    private createGetHeaderOptions(authorizationToken: string, excludeKey: boolean): RequestOptions {
        const headers = new Headers();

        if ( !excludeKey ) {
            headers.append('X-ApiKey', this.apiKey);
        }
        if (authorizationToken) {
            headers.append('Authorization', `Bearer ${authorizationToken}`);
        }
        return new RequestOptions({ headers: headers });
    }

    // Method that creates the header options for post requests
    private createPostAndPutHeaderOptions(authorizationToken: string, contentType: string, excludeKey: boolean): RequestOptions {

        const headers = new Headers();
        headers.append('Content-Type', contentType);

        if (authorizationToken) {
            headers.append('Authorization', `Bearer ${authorizationToken}`);
        }

        if ( !excludeKey ) {
            headers.append('X-ApiKey', this.apiKey);
        }

        return new RequestOptions({ headers: headers });
    }

    // Base get method
    public get(url: string, authorizationToken: string = '', excludeKey: boolean = false): Observable<any> {
        const requestUrl = `${this.apiUrl}/${url}`;
        const options = this.createGetHeaderOptions(authorizationToken, excludeKey);
        return this.http.get(requestUrl, options).timeout(this.timeout);
    }

    // Base post method
    public post(url: string,
                data: any,
                authorizationToken: string = '',
                contentType: string = 'application/json',
                excludeKey: boolean = false): Observable<any> {
        const requestUrl = `${this.apiUrl}/${url}`;
        const options = this.createPostAndPutHeaderOptions(authorizationToken, contentType, excludeKey);
        return this.http.post(requestUrl, data, options).timeout(this.timeout);
    }

    // Base put method
    public put(url: string,
               data: any,
               authorizationToken: string = '',
               contentType: string = 'application/json',
               excludeKey: boolean = false): Observable<any> {
        const requestUrl = `${this.apiUrl}/${url}`;
        const options = this.createPostAndPutHeaderOptions(authorizationToken, contentType, excludeKey);
        return this.http.put(requestUrl, data, options).timeout(this.timeout);
    }
}
