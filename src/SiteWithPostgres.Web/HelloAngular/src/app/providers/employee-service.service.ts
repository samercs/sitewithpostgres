import { Injectable } from '@angular/core';
// RxJS references
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
// tslint:disable-next-line:import-blacklist
import 'rxjs/Rx';
import { HttpClientService } from '../providers/http-client-service';
import { Employee } from '../model/employee.model';
import { retry } from 'rxjs/operators/retry';


@Injectable()
export class EmployeeServiceService {

  constructor(private httpClient: HttpClientService) { }

  public getAll(): Observable<Array<Employee>> {
    return this.httpClient.get('users/get-all', null, false).map(res => res.json());
  }
}
