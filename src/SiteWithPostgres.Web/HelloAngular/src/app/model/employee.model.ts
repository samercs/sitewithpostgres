export class Employee {
  public id: number;
  public name: string;
  public email?: string;
  public gender: string;
  public phoneNumber?: string;
  public dateOfBirth: Date;
  public department: string;
  public photoPath?: string;
}
