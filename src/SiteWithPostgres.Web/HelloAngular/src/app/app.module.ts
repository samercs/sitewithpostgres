import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { EmployeesListComponent } from './employees/employees-list.component';
import { Route } from '@angular/compiler/src/core';

// providers list
import { HttpClientService } from './providers/http-client-service';
import { EmployeeServiceService } from './providers/employee-service.service';
import { HttpModule } from '@angular/http';
import { CreateEmployeeComponent } from './employees/create/create-employee.component';

const appRout: Routes = [
  {path: 'list', component: EmployeesListComponent},
  {path: 'create', component: CreateEmployeeComponent},
  {path: '', redirectTo: '/list', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    EmployeesListComponent,
    CreateEmployeeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRout)
  ],
  providers: [
    HttpClientService,
    EmployeeServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
