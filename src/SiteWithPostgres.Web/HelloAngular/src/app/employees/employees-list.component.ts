import { Component, OnInit } from '@angular/core';
import { Employee } from '../model/employee.model';
import { EmployeeServiceService } from '../providers/employee-service.service';
@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.css']
})
export class EmployeesListComponent implements OnInit {

  public employees: Array<Employee> = [];

  constructor(private empService: EmployeeServiceService) {
    empService.getAll().subscribe(data => {
      this.employees = data;
    });
  }

  ngOnInit() {
  }

}
