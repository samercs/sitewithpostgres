using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(SiteWithPostgres.Web.Startup))]
namespace SiteWithPostgres.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
