using System.Web.Optimization;

namespace SiteWithPostgres.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;

            var jquery = new ScriptBundle("~/js/jquery", "//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js").Include(
                "~/scripts/jquery-{version}.js");
            jquery.CdnFallbackExpression = "window.jQuery";
            bundles.Add(jquery);

            var jqueryui = new ScriptBundle("~/js/jqueryui", "//ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js").Include(
                "~/scripts/jquery-ui-1.8.js");
            jquery.CdnFallbackExpression = "window.jQuery.ui";
            bundles.Add(jqueryui);

            var bootstrap = new ScriptBundle("~/js/bootstrap", "//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js").Include(
                "~/scripts/bootstrap.js");
            bootstrap.CdnFallbackExpression = "$.fn.modal";
            bundles.Add(bootstrap);

            bundles.Add(new ScriptBundle("~/js/site").Include(
                "~/scripts/jquery.validate*",
                "~/scripts/phone-number.js",
                "~/scripts/site.js"));

            bundles.Add(new ScriptBundle("~/js/validate").Include(
                "~/scripts/jquery.validate*"));

            bundles.Add(new StyleBundle("~/css").Include(
                "~/content/site.css",
                "~/content/animate.css"));

            bundles.Add(new StyleBundle("~/css/admin").Include(
                "~/content/kendo/kendo.common.css",
                "~/content/kendo/kendo.css",
                "~/content/site-kendo.css",
                "~/content/site-admin.css"));

            //Angular Bundel

            bundles.Add(new ScriptBundle("~/angularJs")
                .Include("~/AngularBundles/inline.*",
                    "~/AngularBundles/polyfills.*",
                    "~/AngularBundles/scripts.*",
                    "~/AngularBundles/vendor.*",
                    "~/AngularBundles/main.*"));


            bundles.Add(new StyleBundle("~/angularCss")
                .Include("~/AngularBundles/styles.*"));
        }
    }
}
