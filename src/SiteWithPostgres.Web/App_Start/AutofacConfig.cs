using Autofac;
using Autofac.Integration.Mvc;
using SiteWithPostgres.Core.Logger;
using SiteWithPostgres.Core.Service;
using SiteWithPostgres.Data;
using SiteWithPostgres.Web.Core.Configuration;
using SiteWithPostgres.Web.Core.Services;
using System.Configuration;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac.Integration.WebApi;


namespace SiteWithPostgres.Web
{
    public class AutofacConfig
    {
        public static IContainer RegisterAll()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new AutofacWebTypesModule());
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<AppServices>().As<IAppServices>();
            builder.RegisterType<DataContextFactory>().As<IDataContextFactory>();
            builder.RegisterType<AuthService>().As<IAuthService>();
            builder.RegisterType<CookieService>().As<ICookieService>();
            builder.RegisterType<LanguageService>().As<ILanguageService>();

            builder.Register<IAppSettings>(c => new AppSettings(ConfigurationManager.AppSettings));
            builder.Register<IMessageService>(c => new MessageService(c.Resolve<IAppSettings>().EmailSettings));
            builder.RegisterType<TraceErrorLogger>().As<IErrorLogger>();

            return Container(builder);
        }

        private static IContainer Container(ContainerBuilder builder)
        {
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            return container;
        }
    }
}
