namespace SiteWithPostgres.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migration2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Addresses", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.Contacts", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.EmailTemplates", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.Images", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.Links", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.NewsLetterSubscriptions", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.Pages", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.Portfolios", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.PortfolioTypes", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.Quotes", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.Resources", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.Services", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.Skills", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.Slides", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.Statistics", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.Teams", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.AspNetUsers", "CreatedUtc", c => c.DateTime(nullable: false, defaultValueSql: "current_date"));
            AlterColumn("dbo.AspNetUsers", "LockoutEndDateUtc", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "LockoutEndDateUtc", c => c.DateTime());
            AlterColumn("dbo.AspNetUsers", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Teams", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Statistics", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Slides", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Skills", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Services", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Resources", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Quotes", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PortfolioTypes", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Portfolios", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Pages", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.NewsLetterSubscriptions", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Links", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Images", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.EmailTemplates", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Contacts", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Addresses", "CreatedUtc", c => c.DateTime(nullable: false));
        }
    }
}
