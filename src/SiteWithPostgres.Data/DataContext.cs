using Microsoft.AspNet.Identity.EntityFramework;
using SiteWithPostgres.Entities;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Threading.Tasks;

namespace SiteWithPostgres.Data
{
    public class DataContext : IdentityDbContext<User>, IDataContext
    {
        public IDbSet<Address> Addresses { get; set; }
        public IDbSet<EmailTemplate> EmailTemplates { get; set; }
        public IDbSet<Image> Images { get; set; }
        public IDbSet<NewsLetterSubscription> NewsLetterSubscriptions { get; set; }
        public IDbSet<Page> Pages { get; set; }
        public IDbSet<Resource> Resources { get; set; }
        public IDbSet<Slide> Slides { get; set; }
        public IDbSet<Contact> Contacts { get; set; }
        public IDbSet<Service> Services { get; set; }
        public IDbSet<Team> Teams { get; set; }
        public IDbSet<Link> Links { get; set; }
        public IDbSet<Skill> Skills { get; set; }
        public IDbSet<Statistic> Statistics { get; set; }
        public IDbSet<Quote> Quotes { get; set; }

        public IDbSet<Portfolio> Portfolios { get; set; }
        public IDbSet<PortfolioType> PortfolioTypes { get; set; }
        public DataContext() : base("Default")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public static DataContext Create()
        {
            return new DataContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");
            modelBuilder.Properties<decimal>().Configure(prop => prop.HasPrecision(18, 3));
            modelBuilder.Entity<Portfolio>()
                .HasMany(i => i.Types)
                .WithMany(i => i.Portfolios)
                .Map(cs =>
                {
                    cs.MapLeftKey("PortfolioId");
                    cs.MapRightKey("PortfolioTypeId");
                    cs.ToTable("PortfolioTypePortfolios");
                });

            base.OnModelCreating(modelBuilder);
        }

        public void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                TraceValidationErrors(ex);
                throw;
            }
        }

        public override Task<int> SaveChangesAsync()
        {
            try
            {
                return base.SaveChangesAsync();
            }
            catch (DbEntityValidationException ex)
            {
                TraceValidationErrors(ex);
                throw;
            }
        }

        private static void TraceValidationErrors(DbEntityValidationException ex)
        {
            foreach (var validationErrors in ex.EntityValidationErrors)
            {
                foreach (var validationError in validationErrors.ValidationErrors)
                {
                    Trace.TraceError("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                }
            }
        }
    }
}
