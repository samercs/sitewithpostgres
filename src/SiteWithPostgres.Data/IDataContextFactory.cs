
namespace SiteWithPostgres.Data
{
    public interface IDataContextFactory
    {
        IDataContext GetContext();
    }
}
