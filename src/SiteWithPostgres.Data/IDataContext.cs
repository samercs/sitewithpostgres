using SiteWithPostgres.Entities;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace SiteWithPostgres.Data
{
    public interface IDataContext : IDisposable
    {
        IDbSet<Address> Addresses { get; set; }
        IDbSet<EmailTemplate> EmailTemplates { get; set; }
        IDbSet<Image> Images { get; set; }
        IDbSet<NewsLetterSubscription> NewsLetterSubscriptions { get; set; }
        IDbSet<Page> Pages { get; set; }
        IDbSet<Resource> Resources { get; set; }
        IDbSet<Slide> Slides { get; set; }
        IDbSet<Contact> Contacts { get; set; }
        IDbSet<Service> Services { get; set; }
        IDbSet<Link> Links { get; set; }
        IDbSet<Team> Teams { get; set; }
        IDbSet<Skill> Skills { get; set; }
        IDbSet<Statistic> Statistics { get; set; }
        IDbSet<Quote> Quotes { get; set; }
        IDbSet<Portfolio> Portfolios { get; set; }
        IDbSet<PortfolioType> PortfolioTypes { get; set; }




        int SaveChanges();
        Task<int> SaveChangesAsync();
        void SetModified(object entity);

    }
}
