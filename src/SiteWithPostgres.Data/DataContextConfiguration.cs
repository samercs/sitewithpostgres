using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace SiteWithPostgres.Data
{
    public class DataContextConfiguration : DbConfiguration
    {
        public DataContextConfiguration()
        {
            SetExecutionStrategy("System.Data.SqlClient", () => new SqlAzureExecutionStrategy());
        }
    }
}
